<?php
/**
 * Created by PhpStorm.
 * User: leonardolobato
 * Date: 11/04/18
 * Time: 15:18
 */

namespace Infrastructure\Auth\Helpers;


interface IAuth
{
    public function user();
}