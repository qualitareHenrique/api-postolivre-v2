<?php

namespace Infrastructure\Auth;



use Api\Upload\Models\Upload;
use Api\Upload\Transformers\UploadTransformer;
use Api\User\Exceptions\NotConfirmedUserException;
use Api\User\Interfaces\IOauthAccessTokenService;
use Api\User\Interfaces\IOauthClientService;
use Api\User\Interfaces\IUserRepository;
use Api\User\Interfaces\IUserService;
use Carbon\Carbon;
use Illuminate\Contracts\Auth\PasswordBroker;
use Illuminate\Foundation\Application;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Infrastructure\Auth\Exceptions\InvalidCredentialsException;
use Laravel\Passport\Passport;
use League\OAuth2\Server\Exception\OAuthServerException;
use Illuminate\Events\Dispatcher;

class LoginProxy
{
    const REFRESH_TOKEN = 'refreshToken';

    private $auth;

    private $cookie;

    private $db;

    private $request;

    private $userService;

    private $userRepository;

    private $oauthClientService;

    private $oauthAccessTokenService;

    private $passwordBroker;

    private $dispatcher;

    private $password_client_id;

    private $oauth_client_password;


    public function __construct(Application $app,
                                IUserService $userService,
                                IUserRepository $userRepository,
                                IOauthClientService $oauthClientService,
                                IOauthAccessTokenService $oauthAccessTokenService,
                                PasswordBroker $passwordBroker,
                                Dispatcher $dispatcher)
    {
        $this->userService = $userService;
        $this->userRepository = $userRepository;
        $this->oauthClientService = $oauthClientService;
        $this->oauthAccessTokenService = $oauthAccessTokenService;
        $this->passwordBroker = $passwordBroker;
        $this->dispatcher = $dispatcher;

        $this->auth = $app->make('auth');
        $this->cookie = $app->make('cookie');
        $this->db = $app->make('db');
        $this->request = $app->make('request');

        $this->oauth_client_password = $this->oauthClientService->findWhere([
            ['password_client', '=', true],
            ['revoked', '=', false],
//            ['contact_id', '=', null]
        ])->first();

        if(!$this->oauth_client_password){
            throw new \Exception('Authentication Client Not Configured');
        }
    }

    /**
     * Attempt to create an access token using user credentials
     *
     * @param string $email
     * @param string $password
     * @param bool $remember
     * @return array
     * @throws OAuthServerException
     */
    public function attemptLogin($email, $password, $remember = false)
    {
        $user = $this->userRepository->scopeQuery(function ($query) use ($email) {
            return $query->where('email', '=', $email)->orWhere('cnpj', '=', $email);
        })->with(['distributor','station'])->first();

        if (!is_null($user)) {
            if(!$user->confirmed){
                $this->userService->resendConfirmationEmail($user);

                throw new NotConfirmedUserException();
            }

            $credential = $this->proxy('password', [
                'username' => $email,
                'password' => $password,
                'remember' => $remember
            ]);

            $credential['user'] = $user;

            $credential['user']['profilePictureUrl'] = $user->profilePicture != null ? $this->applyTransformerToUserPicture($user->profilePicture->first()):null;

            $credential['user']['permissions'] = collect($user->getAllPermissions())
                ->map(function ($permission) {
                    return $permission->name;
                });
            $credential['user']['roles'] = $user->getRoleNames();

            // Update last Login
            // @TODO: Mudar para event listener
            $this->userRepository->resetScope()->update(['latest_login' => Carbon::now()], $user->id);

            return $credential;
        }

        throw new InvalidCredentialsException();
    }

    public function attempClientLogin($clientSecret, $hours = null)
    {
        return $this->clientProxy($clientSecret, [], $hours);
    }

    private function clientProxy($clientSecret, array $scopes = [], $hours = null)
    {
        $expires_in = config('auth.oauth.access_token_expires_in');
        $client =  $this->oauthClientService->findWhere([['secret', '=', $clientSecret]])->first();

        // remove any previous access token before create a new one
        $this->oauthAccessTokenService->deleteWhere([['client_id', '=', $client->id]]);

        if (empty($client)) {
            throw new InvalidCredentialsException("Contact Does Not Have Oauth Client");
        }

        $data = [
            'client_id' => $client->id,
            'client_secret' => $clientSecret,
            'grant_type' => 'client_credentials',
        ];

        Passport::tokensExpireIn(now()->addHours($hours ? $hours: $expires_in));

        $response = Http::post(\request()->getSchemeAndHttpHost() . '/oauth/token', $data);

        if (!$response->successful()) {
            throw new InvalidCredentialsException();
        }

        $data = json_decode($response->body());


        return [
            'access_token' => $data->access_token,
            'expires_in' => $data->expires_in
        ];
    }

    /**
     * Attempt to refresh the access token used a refresh token that
     * has been saved in a cookie
     */
    public function attemptRefresh()
    {
        $refreshToken = $this->request->cookie(self::REFRESH_TOKEN);

        return $this->proxy('refresh_token', [
            'refresh_token' => $refreshToken
        ]);
    }

    /**
     * Proxy a request to the OAuth server.
     *
     * @param string $grantType what type of grant type should be proxied
     * @param array $data the data to send to the server
     * @return array
     * @throws OAuthServerException
     * @internal param array $scopes
     */
    public function proxy($grantType, array $data = [])
    {
        $expires_in = config('auth.oauth.access_token_expires_in');

        $data = array_merge($data, [
            'client_id' => $this->oauth_client_password->id,
            'client_secret' => $this->oauth_client_password->secret,
            'grant_type' => $grantType
        ]);


        if(isset($data['remember']) && $data['remember']){
            $expires_in = config('auth.oauth.remember_expires_in');
        }

        Passport::tokensExpireIn(now()->addHours($expires_in));

        $response = Http::post(\request()->getSchemeAndHttpHost() . '/oauth/token', $data);

        if (!$response->successful()) {
            if($response->status() == 400){
                throw new OAuthServerException("Enviroment bad configuration is the probable cause of this exception.", 400, 'server_error');
            }
            throw new InvalidCredentialsException();
        }

        $data = json_decode($response->body());
        // Create a refresh token cookie
        $this->cookie->queue(
            self::REFRESH_TOKEN,
            $data->refresh_token,
            1728000, // 10 days
            null,
            null,
            false,
            true // HttpOnly
        );

        return [
            'access_token' => $data->access_token,
            'expires_in' => $data->expires_in
        ];
    }


    /**
     * Logs out the user. We revoke access token and refresh token.
     * Also instruct the client to forget the refresh cookie.
     */
    public function logout()
    {
        $accessToken = $this->auth->user()->token();

        $this->db
            ->table('oauth_refresh_tokens')
            ->where('access_token_id', $accessToken->id)
            ->update([
                'revoked' => true
            ]);

        $accessToken->revoke();

        app()['cache']->forget('spatie.permission.cache');

        $this->cookie->queue($this->cookie->forget(self::REFRESH_TOKEN));
    }

    private function applyTransformerToUserPicture(Upload $profilePicture){
        if($profilePicture){
            $transformer = new UploadTransformer();
            return $transformer->transform($profilePicture)['url'];
        }
    }
}
