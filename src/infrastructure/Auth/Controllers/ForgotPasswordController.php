<?php
/**
 * Created by IntelliJ IDEA.
 * User: stalo
 * Date: 19/02/18
 * Time: 08:38
 */

namespace Infrastructure\Auth\Controllers;

use Api\User\Interfaces\IUserService;
//use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Infrastructure\Http\Controller;

class ForgotPasswordController extends Controller {

//	use SendsPasswordResetEmails;

	protected $userService;

	public function __construct(IUserService $userService)
	{
		$this->userService = $userService;
	}

	public function sendResetLinkEmail(Request $request)
	{
        validate($request,['email' => 'required|email']);
        $this->userService->sendResetLinkEmail($request->all());

        return $this->response(null, 200);
	}

}
