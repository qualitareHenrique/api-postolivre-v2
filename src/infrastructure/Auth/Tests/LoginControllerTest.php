<?php
/**
 * Created by PhpStorm.
 * User: leonardolobato
 * Date: 05/03/18
 * Time: 17:38
 */

namespace Infrastructure\Auth\Tests;

use Api\User\Interfaces\IUserService;
use Api\User\Models\OauthClient;
use Api\User\Models\User;
use Infrastructure\Testing\NilusBaseTest;

class LoginControllerTest extends NilusBaseTest
{
    protected function setUp()
    {
        $this->serviceInterface = IUserService::class;
        $this->model = User::class;

        parent::setUp();
    }

    //TODO: implement this
    public function test_should_login_requires_email_and_password()
    {
        $this->postJson( '/api/v1/login', [], $this->getApiDefaultHeader())
            ->assertStatus(422)
            ->assertJsonStructure(['error' => [
                "code",
                "message",
                "errors" => [
                    "username",
                    "password"
                ]
            ]]);
    }

    public function test_should_login_successfully()
    {
        $payload = ["username" => $this->entityInDatabase->email, "password" => "secret"];
        $headers = ['Content-Type' => 'application/json'];

        $this->postJson('/api/v1/login', $payload, $headers)
            ->assertStatus(200)
            ->assertJsonStructure([
                'access_token',
                'expires_in',
                'permissions'
            ]);

    }

    public function test_should_not_login_if_user_is_not_confirmed()
    {
        $user = factory(User::class)->create(['confirmed' => false]);

        $payload = ["username" => $user->email, "password" => "secret"];
        $headers = ['Content-Type' => 'application/json'];

        $this->postJson('/api/v1/login', $payload, $headers)
            ->assertStatus(401);

    }

}