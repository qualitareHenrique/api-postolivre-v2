<?php

namespace Infrastructure\Auth;

use Illuminate\Support\Carbon;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Laravel\Passport\Passport;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        Passport::routes();

        Passport::tokensExpireIn(Carbon::now()->addDays(10));

        Passport::refreshTokensExpireIn(Carbon::now()->addDays(20));
    }
}