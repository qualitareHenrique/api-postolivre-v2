<?php

use Illuminate\Broadcasting\BroadcastController;

$router->post('/api/v1/login', 'LoginController@login');
$router->post('/api/v1/client', 'LoginController@loginClient');
$router->post('/api/v1/refresh-token', 'LoginController@refresh');
$router->post('/api/v1/forgot-password', 'ForgotPasswordController@sendResetLinkEmail');
$router->post('/api/v1/reset-password', 'ResetPasswordController@reset');
//$router->post('/api/v1/create-account', 'ResetPasswordController@reset');
$router->post('/api/v1/register', 'RegisterController@register');

$router->post('/api/v1/broadcasting/auth', '\\'.BroadcastController::class.'@authenticate');


