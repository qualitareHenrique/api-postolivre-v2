<?php
ini_set('memory_limit', '512M');

defined('API_VERSION') or define("API_VERSION",  config('l5-swagger.constants.VERSION'));

$router->get('/', 'DefaultApiController@index');
$router->get('/api/v1', 'DefaultApiController@index');
