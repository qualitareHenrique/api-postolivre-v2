<?php

namespace Infrastructure\Api\Controllers;

use Api\Auction\Broadcasting\TesteEvent;
use Api\Auction\Interfaces\IAuctionRepository;
use Api\Auction\Models\Auction;
use Api\Bid\Interfaces\IBidRepository;
use Api\Lot\Interfaces\ILotRepository;
use Api\Lot\Models\Lot;
use Api\Unit\Interfaces\IUnitRepository;
use Api\Unit\Interfaces\IUnitService;
use Api\User\Interfaces\IUserRepository;
use Api\User\Interfaces\IUserService;
use Api\User\Mails\UserConfirmation;
use Api\User\Models\User;
use App\Events\OpeningOfAuction;
use App\Notifications\AuctionClose;
use App\Notifications\LotsCreated;
use Carbon\Carbon;
use Illuminate\Contracts\Auth\PasswordBroker as IPasswordBroker;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Query\JoinClause;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Storage;
use Infrastructure\Services\Mail\Providers\SendGrid\SendGridMailService;

class DefaultApiController extends Controller
{

    /**
     * @var SendGridMailService
     */
    private $sendGridMailService;
    private $userRepository;
    private $passwordBroker;
    private $userService;
    private $unitRepository;
    private $lotRepository;
    private $bidRepository;
    private $auctionRepository;

    public function __construct(
            SendGridMailService $sendGridMailService,
            IUserRepository $userRepository,
            IUserService $userService,
            IUnitRepository $unitRepository,
            IPasswordBroker $passwordBroker,
            ILotRepository $lotRepository,
            IBidRepository $bidRepository,
    IAuctionRepository $auctionRepository
    ) {
        $this->auctionRepository = $auctionRepository;
        $this->bidRepository = $bidRepository;
        $this->lotRepository = $lotRepository;
        $this->userService = $userService;
        $this->unitRepository = $unitRepository;
        $this->sendGridMailService = $sendGridMailService;
        $this->userRepository = $userRepository;
        $this->passwordBroker = $passwordBroker;
    }

    public function index()
    {
//        $user = $this->userRepository->first();
//        $user['token'] = $this->passwordBroker->createToken($user);
//
//        Event::dispatch(new UserWasCreated($user));
//        dd('asd');
//        $users = $this->userRepository->scopeQuery(function ($query) {
//            return $query
////                ->whereHas('distributor')
//                ;
//        })->get();
        // Notificação de Criação de Lotes

//        $lotes = collect([
//            new Lot(['id'=> '168aa4e0-c187-11ea-b1e3-d75147666d48']),
//            new Lot(['id'=> '168aa4e0-c187-11ea-b1e3-d75147666d48'])
//        ]);
//        Notification::send($users, new LotsCreated($lotes));
//        $this->userService->sendResetLinkEmail("posto@postolivre.com");
        return response()->json([
            'title'     => config('app.name'),
//            'version'   => trans('misc.version') . ' ' . Version::getGitTag(),
            'locale'    => app()->getLocale(),
            'db status' => User::all()->count() ? 'on': 'off',
            'filesystem' => env('FILESYSTEM_CLOUD'),
        ]);
    }

    /**
     * @OA\OpenApi(
     *     @OA\Info(
     *         version="1.0.0",
     *         title="Swagger Petstore",
     *         description="This is a sample server Petstore server.  You can find out more about Swagger at [http://swagger.io](http://swagger.io) or on [irc.freenode.net, #swagger](http://swagger.io/irc/).  For this sample, you can use the api key `special-key` to test the authorization filters.",
     *         termsOfService="http://swagger.io/terms/",
     *         @OA\Contact(
     *             email="apiteam@swagger.io"
     *         ),
     *         @OA\License(
     *             name="Apache 2.0",
     *             url="http://www.apache.org/licenses/LICENSE-2.0.html"
     *         )
     *     ),
     *     @OA\Server(
     *         description="OpenApi host",
     *         url="https://petstore.swagger.io/v3"
     *     ),
     *     @OA\ExternalDocumentation(
     *         description="Find out more about Swagger",
     *         url="http://swagger.io"
     *     )
     * )
     */

    /**
     * @OA\Get(
     *     path="/pet/findByTags",
     *     summary="Finds Pets by tags",
     *     tags={"pet"},
     *     description="Muliple tags can be provided with comma separated strings. Use tag1, tag2, tag3 for testing.",
     *     operationId="findPetsByTags",
     *     @OA\Parameter(
     *         name="tags",
     *         in="query",
     *         description="Tags to filter by",
     *         required=true,
     *         @OA\Schema(
     *           type="array",
     *           @OA\Items(type="string"),
     *         ),
     *         style="form"
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="successful operation",
     *         @OA\Schema(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/Pet")
     *         ),
     *     ),
     *     @OA\Response(
     *         response="400",
     *         description="Invalid tag value",
     *     ),
     *     security={
     *         {"petstore_auth": {"write:pets", "read:pets"}}
     *     },
     *     deprecated=true
     * )
     */

}

