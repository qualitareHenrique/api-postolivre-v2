<?php

namespace Infrastructure\Http;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Collection;
use Infrastructure\Database\Eloquent\Model;
use Infrastructure\Services\IService;
use Symfony\Component\HttpFoundation\JsonResponse;


abstract class Controller extends \App\Http\Controllers\Controller
{
    protected $service;
    protected $permission;

    protected function response($data, $service = null, $statusCode = 200, array $headers = []){

        if($service && $service instanceof IService){
            $this->service = $service;
        }

        // only apply presenter where object is instance of Model or Collection
        if($data instanceof Model || $data instanceof Collection || $data instanceof LengthAwarePaginator){
            $data = $this->service->parserResult($data);
        }

        return new JsonResponse($data, $statusCode, $headers);
    }
}
