<?php

namespace Infrastructure\Http;

use Fruitcake\Cors\HandleCors;
use Illuminate\Auth\Middleware\Authenticate;
use Illuminate\Auth\Middleware\AuthenticateWithBasicAuth;
use Illuminate\Auth\Middleware\Authorize;
use Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse;
use Illuminate\Foundation\Http\Kernel as HttpKernel;
use Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode;
use Illuminate\Foundation\Http\Middleware\ConvertEmptyStringsToNull;
use Illuminate\Foundation\Http\Middleware\ValidatePostSize;
use Illuminate\Routing\Middleware\SubstituteBindings;
use Illuminate\Routing\Middleware\ThrottleRequests;
use Infrastructure\Http\Middleware\Broadcast;
use Infrastructure\Http\Middleware\Cors;
use Infrastructure\Http\Middleware\EncryptCookies;
use Infrastructure\Http\Middleware\EnforceTenancy;
use Infrastructure\Http\Middleware\XAuthorizationHeader;
use Infrastructure\Http\Middleware\ETag;
use Infrastructure\Http\Middleware\TrimStrings;
use Infrastructure\Http\Middleware\TrustProxies;
use Laravel\Passport\Http\Middleware\CheckClientCredentials;
use Laravel\Passport\Http\Middleware\CheckForAnyScope;
use Laravel\Passport\Http\Middleware\CheckScopes;
use Spatie\Permission\Middlewares\PermissionMiddleware;
use Spatie\Permission\Middlewares\RoleMiddleware;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * These middleware are run during every request to your application.
     *
     * @var array
     */
    protected $middleware = [
        CheckForMaintenanceMode::class,
        ValidatePostSize::class,
        TrimStrings::class,
        EncryptCookies::class,
        AddQueuedCookiesToResponse::class,
        ConvertEmptyStringsToNull::class,
        TrustProxies::class,
        ETag::class,
        HandleCors::class,
        EnforceTenancy::class,
        XAuthorizationHeader::class,
    ];

    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [
        'api' => [
            'throttle:60,1',
            'bindings',
        ],
    ];


    /**
     * The application's route middleware.
     *
     * These middleware may be assigned to groups or used individually.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth' => Authenticate::class,
        'auth.basic' => AuthenticateWithBasicAuth::class,
        'bindings' => SubstituteBindings::class,
        'can' => Authorize::class,
        'throttle' => ThrottleRequests::class,
        'client' => CheckClientCredentials::class,
        'scopes' => CheckScopes::class,
        'scope' => CheckForAnyScope::class,
        'permission' => PermissionMiddleware::class,
        'role'       => RoleMiddleware::class,
    ];
}
