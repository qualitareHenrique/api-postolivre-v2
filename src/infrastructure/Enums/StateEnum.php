<?php


namespace Infrastructure\Enums;


class StateEnum
{
    const AC = 'AC';
    const AL = 'AL';
    const AP = 'AP';
    const AM = 'AM';
    const BA = 'BA';
    const CE = 'CE';
    const DF = 'DF';
    const ES = 'ES';
    const GO = 'GO';
    const MA = 'MA';
    const MT = 'MT';
    const MS = 'MS';
    const MG = 'MG';
    const PA = 'PA';
    const PB = 'PB';
    const PR = 'PR';
    const PE = 'PE';
    const PI = 'PI';
    const RJ = 'RJ';
    const RN = 'RN';
    const RS = 'RS';
    const RO = 'RO';
    const RR = 'RR';
    const SC = 'SC';
    const SP = 'SP';
    const SE = 'SE';
    const TO = 'TO';

    static $siglasEstados = [
        'AC'=>'Acre',
        'AL'=>'Alagoas',
        'AP'=>'Amapá',
        'AM'=>'Amazonas',
        'BA'=>'Bahia',
        'CE'=>'Ceará',
        'DF'=>'Distrito Federal',
        'ES'=>'Espírito Santo',
        'GO'=>'Goiás',
        'MA'=>'Maranhão',
        'MT'=>'Mato Grosso',
        'MS'=>'Mato Grosso do Sul',
        'MG'=>'Minas Gerais',
        'PA'=>'Pará',
        'PB'=>'Paraíba',
        'PR'=>'Paraná',
        'PE'=>'Pernambuco',
        'PI'=>'Piauí',
        'RJ'=>'Rio de Janeiro',
        'RN'=>'Rio Grande do Norte',
        'RS'=>'Rio Grande do Sul',
        'RO'=>'Rondônia',
        'RR'=>'Roraima',
        'SC'=>'Santa Catarina',
        'SP'=>'São Paulo',
        'SE'=>'Sergipe',
        'TO'=>'Tocantins'
    ];

    public static function getEstadoBySigla(string $sigla): string
    {
        return self::$siglasEstados[$sigla];
    }
}
