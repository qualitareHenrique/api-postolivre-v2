<?php

namespace Infrastructure\Enums;

class RoleTypeEnum
{
    const ADMIN = 'admin';
    const STATION = 'gas_station';
    const DISTRIBUTOR = 'distributor';
}
