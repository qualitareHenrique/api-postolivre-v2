<?php


namespace Infrastructure\Enums;

class FreightTypeEnum
{
    const CIF = 'CIF';
    const FOB = 'FOB';

    public static $types = [self::CIF, self::FOB];

}
