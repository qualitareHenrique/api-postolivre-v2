<?php
/**
 * Created by PhpStorm.
 * User: stalo
 * Date: 02/05/19
 * Time: 15:24
 */

namespace Infrastructure\Listeners;


use Api\Task\Events\SubTaskWasCreated;
use Api\Task\Events\SubTaskWasDeleted;
use Api\Task\Events\SubTaskWasUpdated;
use Api\User\Services\UserService;
use Infrastructure\Events\Event;
use Infrastructure\Notification\BaseNilusNotify;
use Infrastructure\Notification\Strategies\CreatedSubtask;
use Infrastructure\Notification\Strategies\DeletedSubtask;
use Infrastructure\Notification\Strategies\CloseOrReopenSubtask;

class NilusNotify extends Event
{
    public $userService;
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function handle($event)
    {
        $notificador = new BaseNilusNotify();
        $eventShortName = get_class($event);
        switch ($eventShortName) {

        }
    }
}
