<?php

namespace Infrastructure\Listeners;

use Api\Task\Models\SubTask;
use Api\Task\Models\Task;
use Api\User\Models\Activity;
use Api\User\Models\User;
use Illuminate\Queue\InteractsWithQueue;
use ReflectionClass;

/**
 * Created by PhpStorm.
 * User: leonardolobato
 * Date: 12/03/18
 * Time: 10:53
 */
class LogUserActivity
{
    use InteractsWithQueue;

    public function handle($event)
    {
        $param = (new ReflectionClass(get_class($event)))->getConstructor()->getParameters()[0]->name;
        $user = auth()->user();

        if(!$user) {
            $this->logAsAnonymous($event, $param);
            return;
        }

        $morphFields = $this->getMorphField($event, $param);

        if($morphFields){
            $this->morphActivity($event, $morphFields, $param, $user);
        }else{
            activity($user ? $user->first_name . ' | ' . $user->email : null)
                ->causedBy($user)
                ->performedOn($event->{$param})
                ->withProperties(['entity' => $event->{$param}->keys] ?: [])
                ->log($event->{$param}->action ?: '');
        }
    }

    private function logAsAnonymous($event, $param) {
        activity('user not logged')
            ->performedOn($event->{$param})
            ->withProperties(['entity' => $event->{$param}->keys] ?: [])
            ->log($event->{$param}->action ?: '');
    }

    private function getMorphField($event, $param){
        $model = $event->{$param};

        return [
            'id' => $model->{$param . 'able_id'},
            'model' => $model->{$param . 'able_type'}
        ];
    }

    /**
     * @param $event
     * @param $morphFields
     * @param $param
     * @param $user
     */
    private function morphActivity($event, $morphFields, $param, $user): void
    {
        $model = $event->{$param};
        $changed = $model->getChanges();


        $properties = [
            'entity' => $model->keys
        ];

        if($changed){
            unset($changed['updated_at']);
            $properties['entityChanges'] = $changed;
        }


        $activity = new Activity();

        $activity->subject_id = $model->id;//
        $activity->subject_type = get_class($model);//

        $activity->causer_id = $user->id;
        $activity->causer_type = User::class;

        $activity->log_name = $user ? $user->first_name . ' | ' . $user->email : null;
        $activity->description = $model->action;
        $activity->properties = $properties;

        $activity->save();
    }

}
