<?php

namespace Infrastructure\Services\Mail\Providers\SendGrid;

use Infrastructure\Services\Mail\IMailService;
use Infrastructure\Services\Mail\IMailServiceBuilder;
use SendGrid\Mail\From;
use SendGrid\Mail\Mail;

class SendGridMailService implements IMailService
{
    private $templateId;
    private $to;
    private $from;
    private $subject;
    private $mailableTemplate;
    private $headers;
    private $attachment;
    private $attachment_url;
    private $attr;
    private $replyto;
    private $bcc;
    private $cc;

    private $mailin;

    public function __construct(SendGridMailServiceBuilder $builder)
    {
        $this->templateId = $builder->getTemplateId();
        $this->to = $builder->getTo();
        $this->from = $builder->getFrom();
        $this->subject = $builder->getSubject();
        $this->mailableTemplate = $builder->getMailableTemplate();
        $this->headers = $builder->getHeaders();
        $this->attachment = $builder->getAttachment();
        $this->attachment_url = $builder->getAttachmentUrl();
        $this->attr = $builder->getAttr();
        $this->replyto = $builder->getReplyto();
        $this->bcc = $builder->getBcc();
        $this->cc = $builder->getCc();

        $this->mailin = new \SendGrid(config('services.mail.sendgrid.api_key'));
    }

    public function send()
    {
        $email = new Mail();
        $email->setFrom(key($this->from), $this->from[key($this->from)]);
        $email->setSubject($this->subject);
        $email->addTo(key($this->to), $this->to[key($this->to)]);
        if ($this->replyto) {
            $email->setReplyTo($this->replyto);
        }

        if ($this->templateId) {
            $email->setTemplateId($this->templateId);
        }

        $email->addContent('text/html', $this->mailableTemplate->render());

        return $this->mailin->send($email);
    }

    static function createBuilder(): IMailServiceBuilder
    {
        return new SendGridMailServiceBuilder();
    }
}
