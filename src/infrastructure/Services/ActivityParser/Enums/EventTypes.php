<?php

namespace Infrastructure\Services\ActivityParser\Enums;

abstract class EventTypes
{
    const UPDATE    = ['Updated', 'updated', 'update', 'updat'];
    const CREATION  = ['Created', 'created', 'create', 'creat'];
    const DELETION  = ['Deleted', 'deleted', 'delete', 'delet'];
}