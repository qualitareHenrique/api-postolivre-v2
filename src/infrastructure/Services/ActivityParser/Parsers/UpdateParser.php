<?php

namespace Infrastructure\Services\ActivityParser\Parsers;

use Api\User\Models\Activity;

class UpdateParser extends BaseParser implements IParser
{

    protected $activity;

    function __construct(Activity $activity)
    {
        $this->activity = $activity;
    }

    public function get(): string
    {
        $subject = parent::subject();
        $type = strtolower(entity($subject));

        $context = parent::context()['original'];

        if($this->fieldsChanged()){
            return trans("activities/messages.{$context}.{$type}.update.0", $this->placeholders());
        }else{
            return trans("activities/messages.unknown", $this->placeholders());
        }
    }

    function changes()
    {
        if($this->activity->properties)
            return $this->activity->getExtraProperty('entityChanges');
    }

    function fieldsChanged()
    {
        $and = trans('activities/messages.and');

        if($this->changes()){

            $translatedKeys = $this->translatedKeys();

            switch (count($translatedKeys)){
                case 1:
                    $fields = implode("", $translatedKeys);
                    break;
                case 2:
                    $fields = implode(" {$and} ", $translatedKeys);
                    break;
                default:{
                    $fields = $this->formatMoreThanTwoFields($translatedKeys, $and);
                }
            }


            return $fields;
        }
    }

    private function placeholders() {

        return [
            'FIELDS' => $this->fieldsChanged(),
            'CONTEXT'=> $this->context()['trans'],
            'CAUSER' => $this->causer()->name
        ];
    }

    private function formatMoreThanTwoFields($fields, $and)
    {
        $implode = implode(", ", $fields);
        $implode =  preg_replace("/,([^,]+)$/", "{$and} $1", $implode);

        return $implode;
    }

    private function translatedKeys()
    {
        $context = $this->context()['original'];
        $keys = array_keys($this->changes());
        $translatedKeys = [];

        foreach($keys as $key){
            $translatedKeys[] = trans("activities/models.{$context}.{$key}");
        }

        return $translatedKeys;
    }

}