<?php
/**
 * Created by PhpStorm.
 * User: leonardolobato
 * Date: 02/07/18
 * Time: 09:59
 */

namespace Infrastructure\Services\ActivityParser\Parsers;


class UnknownParser implements IParser
{
    public function get(): string
    {
        return 'unknown';
    }

    function context()
    {
        return 'no context';
    }

    function causer()
    {
        return 'no owner';
    }

    function subject()
    {
        return 'one entity';
    }
}