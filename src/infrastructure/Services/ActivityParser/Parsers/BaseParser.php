<?php
/**
 * Created by PhpStorm.
 * User: leonardolobato
 * Date: 02/07/18
 * Time: 15:02
 */

namespace Infrastructure\Services\ActivityParser\Parsers;


use Illuminate\Support\Facades\App;

abstract class BaseParser implements IParser
{
    protected $activity;

    function context()
    {
        // get context name and it translation
        $entity = strtolower(entity($this->activity->context_type));
        return ['original' => $entity, 'trans' => trans("activities/models.{$entity}.model")];
    }

    function causer()
    {
        // get model - usually user
        return App::make($this->activity->causer_type)->find($this->activity->causer_id);
    }

    function subject()
    {
        // get model
        $model = App::make($this->activity->subject_type);

        // check if model has softdelete
        if(method_exists($model, 'trashed')){
            $model = $model->withTrashed();
        }

        // return model
        return $model->find($this->activity->subject_id);
    }

    public function get(): string
    {
        // if this method is not overrided than default message is shown
        return  trans('activities/messages.unknown', ['CAUSER' => $this->causer()->name]);
    }

}