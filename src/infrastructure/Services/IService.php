<?php
/**
 * Created by PhpStorm.
 * User: leonardolobato
 * Date: 16/01/18
 * Time: 19:12
 */

namespace Infrastructure\Services;

use Closure;
use Infrastructure\Database\Eloquent\Model;

interface IService
{
    public function paginate($limit = 20);

    public function all();

    public function find($id);

    public function findByField($field, $value = null, $columns = ['*']);

    public function findWhere(array $where, $columns = ['*']);

    public function scopeQuery(Closure $scope);

    public function pushCriteria($mixed);

    public function popCriteria($mixed);

    public function skipCriteria($mixed);

    public function skipPresenter($status = true);

    public function setPresenter($mixed);

    public function create(array $data, Model $model = null, $relation = null);

    public function with($relations);

    public function update(array $data, $id);

    public function updateWhere(array $where, $data = []);

    public function delete($id);

    public function restore($id);

    public function deleteWhere(array $where);

    public function parserResult($result);

    public function runService(Closure $closure, $eventModel);
}
