<?php

namespace Infrastructure\Services\Upload\Providers\Local;

use Api\Upload\Models\Upload;
use Illuminate\Support\Facades\Storage;
use Infrastructure\Services\Upload\Providers\AbstractUploadFileService;
use Infrastructure\Services\Upload\Providers\IUploadFileService;
use Infrastructure\Services\Upload\Providers\IUploadFileServiceBuilder;
use Intervention\Image\Facades\Image;

class LocalUploadFileService extends AbstractUploadFileService implements IUploadFileService
{
    protected $provider =  'local';

    static function createBuilder(): IUploadFileServiceBuilder
    {
        return new LocalUploadFileServiceBuilder();
    }

    public function save()
    {
//        if(is_image($this->file)) {
//
//            $path = $this->path($this->permission[(int)$this->is_public], $this->filename, false);
//            $processedImage = $this->saveImage($this->file, $path, $this->width, $this->height);

//        }else{

            $path = $this->path($this->permission[(int)$this->is_public]);

            Storage::putFileAs($path, $this->file, $this->filename);
 //       }

        return [
            'id' => $this->id,
            'uuid' => $this->uuid,
            'user_id' => $this->user_id,
            'type' => $this->type,
            'title' => $this->title,
            'description' => $this->description,
            'filename' => $this->filename,
            'mime' => $this->mime,
            'extension' => $this->extension,
            'provider' => $this->provider,
            'size' => isset($processedImage) ? $processedImage->filesize() : $this->size ,
            'is_public' => (boolean) $this->is_public
        ];
    }

    public function move($upload)
    {
        $filename = $upload->uuid . $upload->extension;

        $path = $this->path($this->permission[(int)$upload->is_public], $filename, false);

        if(!file_exists($path)){

            Storage::move(
                $this->path($this->permission[(int)!$upload->is_public], $filename),
                $this->path($this->permission[(int)$upload->is_public], $filename));
        }
    }

    public function delete(Upload $upload)
    {
        $filename = $upload->uuid . $upload->extension;
        $path = $this->path($this->permission[(int)$upload->is_public], $filename, false);

        if(file_exists($path))
            unlink($path);
    }

    private function saveImage($file, $path, $width, $height)
    {
        $image = Image::make($file);

        $image->resize(  $width ?? $image->width(),
            $height ?? $image->height(),
            function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });

        return $image->save($path);
    }


    private function path($dir, $filename = null, $storageContext = true)
    {
        if(!$storageContext){
            $dir = $dir . '-app';
        }

        switch ($dir)
        {
            case 'public':
                return 'public/uploads' . DIRECTORY_SEPARATOR. $filename;
                break;

            case 'public-app':
                return upload_path('', true) . $filename;
                break;

            case 'private':
                return 'uploads' . DIRECTORY_SEPARATOR. $filename;
                break;

            case 'private-app':
                return upload_path('', false) . $filename;
                break;

            default:
                return '';
                break;
        }
    }


}