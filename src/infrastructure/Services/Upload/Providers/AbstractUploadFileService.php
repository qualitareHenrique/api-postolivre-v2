<?php

namespace Infrastructure\Services\Upload\Providers;

abstract class AbstractUploadFileService
{
    public static $TENANTS = 'tenants';
    protected $provider = 'local';

    protected $id;
    protected $uuid;
    protected $title;
    protected $description;
    protected $file;
    protected $filename;
    protected $user_id;
    protected $type;
    protected $is_public;
    protected $width;
    protected $height;
    protected $size;
    protected $mime;
    protected $extension;


    protected $permission = [
        0 => 'private',
        1 => 'public'
    ];

    abstract static function createBuilder(): IUploadFileServiceBuilder;

    public function __construct(IUploadFileServiceBuilder $builder)
    {
        $this->id = $builder->getId();
        $this->uuid = $builder->getUuid();
        $this->title = $builder->getTitle();
        $this->description = $builder->getDescription();
        $this->file = $builder->getFile();
        $this->filename = $builder->getFilename();
        $this->user_id = $builder->getUserId();
        $this->type = $builder->getType();
        $this->is_public = $builder->getIsPublic();
        $this->width = $builder->getWidth();
        $this->height = $builder->getHeight();
        $this->size = $builder->getSize();
        $this->mime = $builder->getMime();
        $this->extension = $builder->getExtension();
    }
}