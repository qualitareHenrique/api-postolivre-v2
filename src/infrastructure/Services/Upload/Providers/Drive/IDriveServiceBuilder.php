<?php

namespace Infrastructure\Services\Upload\Providers\Drive;

interface IDriveServiceBuilder
{
    /**
     * @return mixed
     */
    public function getId();

    /**
     * @return mixed
     */
    public function getFilename();


    public function build(): IDriveService;
}