<?php
/**
 * Created by PhpStorm.
 * User: leonardolobato
 * Date: 05/04/18
 * Time: 14:08
 */

namespace Infrastructure\Services\Upload\Providers;

use Api\Upload\Models\Upload;

interface IUploadFileService
{
    public function save();
    public function move($upload);

    public function delete(Upload $upload);
    static function createBuilder():IUploadFileServiceBuilder;
}