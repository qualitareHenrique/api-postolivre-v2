<?php
/**
 * Created by IntelliJ IDEA.
 * User: stalo
 * Date: 15/02/18
 * Time: 18:05
 */

namespace Infrastructure\Services\Upload\Providers\Amazon;

use Infrastructure\Services\Upload\Providers\IUploadFileService;
use Infrastructure\Services\Upload\Providers\IUploadFileServiceBuilder;
use Webpatser\Uuid\Uuid;

class S3UploadFileServiceBuilder implements IUploadFileServiceBuilder
{

    private $id;
    private $uuid;
    private $title;
    private $description;
    private $file;
    private $filename;
    private $user_id;
    private $type;
    private $is_public;
    private $width;
    private $height;
    private $url;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getUuid()
    {
        return $this->setUuid()->uuid;
    }

    /**
     * @return $this
     * @internal param mixed $id
     */
    public function setUuid()
    {
        $this->uuid = $this->uuid ? $this->uuid: Uuid::generate()->string;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @return $this
     * @internal param mixed $id
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
        return $this;
    }


    /**
     * @return mixed
     */
    public function getTitle()
    {
        if($this->title == null)
            if($this->getFile())
                return $this->getFile()->getClientOriginalName();

        return $this->title;
    }

    /**
     * @param mixed $title
     * @return $this
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param mixed $file
     * @return $this
     */
    public function setFile($file)
    {
        $this->file = $file;
        $this->file->filename = $this->getUuid() . ext_from_mime($this->getMime());

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFilename()
    {
        if($this->getFile())
            return $this->getFile()->filename;

        return null;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsPublic()
    {
        return $this->is_public;
    }

    /**
     * @param mixed $is_public
     * @return $this
     */
    public function setIsPublic($is_public)
    {
        $this->is_public = $is_public;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * @param mixed $width
     * @return $this
     */
    public function setWidth($width)
    {
        $this->width = $width;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * @param mixed $height
     * @return $this
     */
    public function setHeight($height)
    {
        $this->height = $height;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSize()
    {
        if($this->getFile())
            return $this->getFile()->getSize();

        return null;
    }


    /**
     * @return mixed
     */
    public function getMime()
    {
        if($this->getFile())
            return $this->getFile()->getClientMimeType();

        return null;
    }

    /**
     * @return mixed
     */
    public function getExtension()
    {
        if($this->getFile())
            return ext_from_mime($this->getFile()->getMimeType());

        return null;
    }

    public function build(): IUploadFileService
    {
        return new S3UploadFileService($this);
    }
}