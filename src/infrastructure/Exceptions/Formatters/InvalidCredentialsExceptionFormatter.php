<?php
namespace Infrastructure\Exceptions\Formatters;

use Exception;
use Illuminate\Http\JsonResponse;
use Infrastructure\Exceptions\BaseFormatter;

class InvalidCredentialsExceptionFormatter extends BaseFormatter
{
    public function format(JsonResponse $response, Exception $ex, array $reporterResponses)
    {
        $statusCode = 404;

        $response->setStatusCode($statusCode);

        $this->debug ?
            $response->setData(error($ex, $statusCode)) :
            $response->setData(error($ex, $statusCode, trans('auth.invalid_credentials')));
    }
}
