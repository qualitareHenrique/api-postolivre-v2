<?php

namespace Infrastructure\Exceptions;

use Throwable;
use Illuminate\Http\JsonResponse;

class ResponseFactory
{
    public static function make(Throwable $e)
    {
        return new JsonResponse([
            'status' => $e->getMessage()
        ]);
    }
}
