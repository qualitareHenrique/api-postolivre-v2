<?php

namespace Infrastructure\Database\Eloquent;

use Infrastructure\Criterias\EagerLoadCriteria;
use Infrastructure\Database\IRepository;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;
use Illuminate\Container\Container as Application;
use Prettus\Repository\Traits\CacheableRepository;

abstract class Repository extends BaseRepository implements CacheableInterface, IRepository
{
    use CacheableRepository;

    protected $eagerLoad = [];

    protected $skipPresenter = true;

    public function __construct(Application $app)
    {
        parent::__construct($app);
        $this->pushCriteria(new EagerLoadCriteria($this->eagerLoad));
    }

    public function boot(){
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function increment(array $columns = ['id'])
    {
        $object = $this->model->withTrashed()->get(['identifier'])->max();
        if(!$object){
            $number = 0;
        }else{
            $clean = explode(".", $object['identifier'])[0];
            $number = preg_replace("/[^0-9]/", "",$clean) ;
        }

        return $number + 1;
    }

    public function withTrashed(): Repository {
        $this->model = $this->model->withTrashed();
        return $this;
    }

    /**
     * update multiple records
     *
     * @param array $where
     * @param array $data
     * @return mixed
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     * @internal param array $columns
     */
    public function updateWhere(array $where, $data = [])
    {
        $this->applyCriteria();
        $this->applyScope();

        $this->applyConditions($where);

        $model = $this->model->update($data);
        $this->resetModel();

        return $this->parserResult($model);
    }

}
