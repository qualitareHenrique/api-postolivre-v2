<?php
/**
 * Created by PhpStorm.
 * User: leonardolobato
 * Date: 18/09/17
 * Time: 20:03
 */

namespace Infrastructure\Criterias;


use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class EagerLoadCriteria implements CriteriaInterface
{
    protected $relations;

    function __construct(array $relations)
    {
        $this->relations = $relations;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        return $model->with($this->relations);
    }
}