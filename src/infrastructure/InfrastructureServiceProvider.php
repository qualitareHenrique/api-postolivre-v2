<?php

namespace Infrastructure;

use Infrastructure\Providers\EventServiceProvider;
use Infrastructure\Services\Mail\IMailService;
use Infrastructure\Services\Mail\IMailServiceBuilder;
use Infrastructure\Services\Mail\Providers\Local\LocalMailService;
use Infrastructure\Services\Mail\Providers\Local\LocalMailServiceBuilder;
use Infrastructure\Services\Mail\Providers\SendGrid\SendGridMailService;
use Infrastructure\Services\Mail\Providers\SendGrid\SendGridMailServiceBuilder;
use Infrastructure\Services\Mail\Providers\SendInBlue\SendInBlueMailService;
use Infrastructure\Services\Mail\Providers\SendInBlue\SendInBlueMailServiceBuilder;
use Infrastructure\Services\Upload\Providers\Amazon\S3UploadFileService;
use Infrastructure\Services\Upload\Providers\Amazon\S3UploadFileServiceBuilder;
use Infrastructure\Services\Upload\Providers\IUploadFileService;
use Infrastructure\Services\Upload\Providers\IUploadFileServiceBuilder;
use Infrastructure\Services\Upload\Providers\Drive\DriveService;
use Infrastructure\Services\Upload\Providers\Drive\DriveServiceBuilder;
use Infrastructure\Services\Upload\Providers\Drive\IDriveService;
use Infrastructure\Services\Upload\Providers\Drive\IDriveServiceBuilder;
use Infrastructure\Services\Upload\Providers\Dropbox\DropboxService;
use Infrastructure\Services\Upload\Providers\Dropbox\DropboxServiceBuilder;
use Infrastructure\Services\Upload\Providers\Dropbox\IDropboxService;
use Infrastructure\Services\Upload\Providers\Dropbox\IDropboxServiceBuilder;
use Infrastructure\Services\Upload\Providers\Local\LocalUploadFileService;
use Infrastructure\Services\Upload\Providers\Local\LocalUploadFileServiceBuilder;

/**
 * Class InfrastructureServiceProvider
 * @package Infrastructure
 */
class InfrastructureServiceProvider extends EventServiceProvider
{
    public function register()
    {

       switch (config('mail.driver', 'local')){
           case 'sendinblue':
               $this->app->bind(IMailService::class,SendInBlueMailService::class);
               $this->app->bind(IMailServiceBuilder::class,SendInBlueMailServiceBuilder::class);
               break;
           case 'sendgrid':
               $this->app->bind(IMailService::class,SendGridMailService::class);
               $this->app->bind(IMailServiceBuilder::class,SendGridMailServiceBuilder::class);
               break;
           default:
               $this->app->bind(IMailService::class,LocalMailService::class);
               $this->app->bind(IMailServiceBuilder::class,LocalMailServiceBuilder::class);
               break;

       }

       switch(config('filesystems.cloud', 'local')){
           case 's3':
               $this->app->bind(IUploadFileService::class,S3UploadFileService::class);
               $this->app->bind(IUploadFileServiceBuilder::class,S3UploadFileServiceBuilder::class);
               break;
           default:
               $this->app->bind(IUploadFileService::class,LocalUploadFileService::class);
               $this->app->bind(IUploadFileServiceBuilder::class,LocalUploadFileServiceBuilder::class);
               break;
       }


        $this->app->bind(IDriveService::class,DriveService::class);
        $this->app->bind(IDriveServiceBuilder::class,DriveServiceBuilder::class);

        $this->app->bind(IDropboxService::class,DropboxService::class);
        $this->app->bind(IDropboxServiceBuilder::class,DropboxServiceBuilder::class);

//        $this->app->bind('ConsoleOutput', \Symfony\Component\Console\Output\ConsoleOutput::class);
        $this->app->singleton('consoleOutput', function () {
            return new \Symfony\Component\Console\Output\ConsoleOutput();
        });
    }
}
