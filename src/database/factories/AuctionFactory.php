<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Api\Auction\Models\Auction;
use Faker\Generator as Faker;
use Carbon\Carbon;

$factory->define(Auction::class, function (Faker $faker) {
    static $identifier, $port_id, $station_id, $fuel_id, $created_at;

    return [
        'identifier' => $identifier,
        'freight_type' => random_int(0,1) ? 'CIF': 'FOB',
        'fuel_amount' => random_int(1000, 10000),
        'date_start' => Carbon::now()->format('Y-m-d 12:00:00'),
        'date_finish' => Carbon::now()->addDay()->format('Y-m-d 09:00:00'),
        'station_id' => $station_id,
        'fuel_id' => $fuel_id,
        'port_id' => $port_id,
        'created_at' => $created_at
    ];
});
