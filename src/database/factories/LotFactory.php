<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Api\Lot\Models\Lot;
use Faker\Generator as Faker;

$factory->define(Lot::class, function (Faker $faker) {
    static $port_id, $fuel_id;

    return [
        'fuel_id' => $fuel_id,
        'port_id' => $port_id
    ];
});
