<?php

use Illuminate\Database\Seeder;
use Api\Fuel\Services\FuelService;
use Illuminate\Support\Str;

class FuelSeeder extends Seeder
{
    private $fuelService;
    public function __construct(FuelService $fuelService)
    {
        $this->fuelService = $fuelService;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $fuels = [
            'GASOLINA COMUM',
            'GASOLINA ADITIVADA',
            'ETANOL',
            'DIESEL S10',
            'DIESEL S500'
        ];

        $fuelsColor = [
            '#FAA700',
            '#01A39D',
            '#8D99AE',
            '#000000',
            '#E41414'
        ];

        foreach ($fuels as $key => $fuel) {
            $this->fuelService->create([
                'name' => $fuel,
                'color_hex' => $fuelsColor[$key],
                'slug'  => Str::slug($fuel, '-')
            ]);
        }
    }
}
