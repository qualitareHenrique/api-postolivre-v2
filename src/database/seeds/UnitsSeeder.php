<?php

use Illuminate\Database\Seeder;
use Api\Unit\Services\UnitService;

class UnitsSeeder extends Seeder
{
    private $unitService;

    public function __construct(UnitService $unitService)
    {
        $this->unitService = $unitService;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $unidades = [
            [
                'state_abbreviation' => 'PB',
                'name' => 'Unidade da Paraíba',
                'email'=> 'unidadepb@postolivre.com',
                'phone'=> '(83) 991881722',
                'cpf_or_cnpj' => '72.091.599/0001-27',
            ],
            [
                'state_abbreviation' => 'PE',
                'name' => 'Unidade de Pernambuco',
                'email'=> 'unidadepe@postolivre.com',
                'phone'=> '(83) 991881744',
                'cpf_or_cnpj' => '44.767.889/0001-02',
            ],
            [
                'state_abbreviation' => 'RN',
                'name' => 'Unidade do Rio Grande do Norte',
                'email'=> 'unidadern@postolivre.com',
                'phone'=> '(83) 991881733',
                'cpf_or_cnpj' => '04.846.640/0001-78',
            ]
        ];
        //factory(\Api\Unit\Models\Unit::class, 10)->create();
        foreach ($unidades as $unidade) {
            $this->unitService->create($unidade);
        }
    }
}
