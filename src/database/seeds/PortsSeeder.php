<?php

use Illuminate\Database\Seeder;
use Api\Port\Interfaces\IPortService;
use Infrastructure\Enums\StateEnum;
use Api\Unit\Interfaces\IUnitService;

class PortsSeeder extends Seeder
{
    private $portService;
    private $unitService;

    public function __construct(IPortService $portService, IUnitService $unitService)
    {
        $this->portService = $portService;
        $this->unitService = $unitService;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $unidadePB = $this->unitService->findWhere(['state_abbreviation' => StateEnum::PB])->first();
        $unidadePE = $this->unitService->findWhere(['state_abbreviation' => StateEnum::PE])->first();
        $unidadeRN = $this->unitService->findWhere(['state_abbreviation' => StateEnum::RN])->first();

        $portos = [
            [
                'name' => 'Porto de Cabedelo',
                'state_abbreviation' => StateEnum::PB,
                'unit_id' => $unidadePB->id
            ],
            [
                'name' => 'Porto de Suape',
                'abbreviation' => StateEnum::PE,
                'unit_id' => $unidadePE->id
            ],
            [
                'name' => 'Porto de Guamaré',
                'abbreviation' => StateEnum::RN,
                'unit_id'   => $unidadeRN->id
            ]
        ];

        foreach ($portos as $porto) {
            $this->portService->create($porto);
        }

    }
}
