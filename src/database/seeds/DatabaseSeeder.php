<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Artisan;
use Infrastructure\Facades\ConsoleOutput;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        /**
         * https://laracasts.com/discuss/channels/laravel/fillable-and-creat-not-working-as-intended
         * Tive problemas com fillable nos seeders usando os services
         */
        \Infrastructure\Database\Eloquent\Model::reguard();

        $this->call(UnitsSeeder::class);
        $this->call(RolesAndPermissionsSeeder::class);
        $this->call(UsersSeeder::class);
        $this->call(FuelSeeder::class);
        $this->call(PortsSeeder::class);
        $this->call(QuestionsSeeder::class);
//        $this->call(AnswersSeeder::class);

        $this->call(AuctionsSeeder::class);
        $this->call(BidsSeeder::class);


        Artisan::call(
            'passport:install',
            [],
            ConsoleOutput::getFacadeRoot()
        );
    }
}
