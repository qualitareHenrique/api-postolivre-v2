<?php

namespace App\Console\Commands;

use Api\Auction\Interfaces\IAuctionRepository;
use Api\Auction\Models\Auction;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Notification;

class PaymentSubmissionRequest extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notify:payment_submission_request';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Notifica os postos que não enviaram o comprovante de pgto';

    private $auctionRepository;
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(IAuctionRepository $auctionRepository)
    {
        $this->auctionRepository = $auctionRepository;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Recupera todos os pedidos (Leilões) que estão associados a um lote
        // criado no dia anterior e que estão sem comprovante de pagamento
        $auctions = $this->auctionRepository->scopeQuery(function (Builder $query) {
            return $query
                ->join('lots', 'lots.id', '=', 'auctions.lot_id')
                ->whereDoesntHave('uploads')
                ->whereDate('lots.created_at', '=', Carbon::now()->subDays(2)->format('Y-m-d'))
                ->select('auctions.*')
                ;
        })->get();

        collect($auctions)->each(function (Auction $auction) {
            Notification::send($auction->station->user, new \App\Notifications\PaymentSubmissionRequest());
        });
    }
}
