<?php

namespace App\Console\Commands;

use Api\Bid\Interfaces\IBidRepository;
use Api\Lot\Interfaces\ILotRepository;
use Api\Lot\Models\Lot;
use App\Notifications\AuctionWinner as AuctionWinnerNotification;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;

class AuctionWinner extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notify:auction_winner';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Notifica as distribuidoras que ganharam o leilão (LOTE)';

    private $lotRepository;
    private $bidRepository;
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(ILotRepository $lotRepository, IBidRepository $bidRepository)
    {
        parent::__construct();
        $this->bidRepository = $bidRepository;
        $this->lotRepository = $lotRepository;

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @noinspection PhpParamsInspection
     */
    public function handle()
    {
        Lot::where('status', 'EM ANDAMENTO')->update(['status' => 'EM NEGOCIAÇÃO']);

        $subquery = DB::table('bids')
            ->selectRaw('MIN(bids.value) as bid, bids.lot_id, bids.id')
            ->where('created_at', '>=', Carbon::now()->subDay()->format('Y-m-d 17:00:00'))
            ->where('created_at', '<=', Carbon::now()->format('Y-m-d 09:00:00'))
            ->groupBy('bids.lot_id');

        // Recuperar os lotes do dia anterior
        $bids = $this->bidRepository->scopeQuery(function (Builder $query) use($subquery) {
            return $query->joinSub($subquery, 'bidsWinner', function ($join) {
                return $join->on('bidsWinner.id','=', 'bids.id');
            })
                ->groupBy('bids.lot_id')
                ->selectRaw('bids.id, bids.lot_id, bids.distributor_id, bidsWinner.bid');
        })->get();

        $bids->each(function ($bidWinner){
            Notification::send(
                $bidWinner->distributor->user,
                new AuctionWinnerNotification($bidWinner)
            );
        });

    }
}
