<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\Log;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        /* Quando pedidos são criados por postos, entram com o status SOLICITADO
         * As 17 horas o sistema cria os lotes para dar início ao leilão e
         * altera seus status para EM ANDAMENTO
         */
        $schedule
            ->command('lots:generator')
            ->dailyAt('17:00')
            ->onSuccess(function () {
                Log::alert("SUCESSO, LOTES GERADOS");
            })->onFailure(function () {
                Log::alert("ERROR NO LOTS GENERATOR");
            });

        /* As 09h os leilões são encerrados e seus status são alterados para
         * EM NEGOCIACAO
         */
        $schedule
            ->command('notify:auction_winner')
            ->dailyAt('09:00');

        /* As 13h o sistema notifica os postos que ainda não submeteram o comprovante
         * de pagamento
         */
        $schedule
            ->command('notify:payment_submission_request')
            ->dailyAt('13:00');

        /* As 15h o sistema encerra os leiloes e altera seus status para FINALIZADO
         */
        $schedule
            ->command('notify:auction_close')
            ->dailyAt('15:00');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
