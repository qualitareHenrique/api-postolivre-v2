<?php

namespace App\Notifications;

use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class OutdatedBid extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['broadcast', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    public function toBroadcast($notifiable)
    {
        return [
            'id' => $this->id,
            'read_at' => null,
            'data' => [
                'title' => 'Seu lance foi ultrapassado',
                'message' => 'Você não solicitou pedidos hoje. Caso queira solicitar, ainda dá tempo!',
                //'message' => "Vamos mudar isso! Faça um novo lance e continue neste leilão"
            ],
            'created_at' => Carbon::now()
        ];
    }

    public function toDatabase() {
        return [
            'title' => 'Seu lance foi ultrapassado',
            'message' => 'Você não solicitou pedidos hoje. Caso queira solicitar, ainda dá tempo!',
            //'message' => "Vamos mudar isso! Faça um novo lance e continue neste leilão"
        ];
    }
}
