<?php

namespace App\Notifications;

use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Collection;

class LotsCreated extends Notification
{
    use Queueable;

    private $lotes;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Collection $lotes)
    {
        $this->lotes = $lotes;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     *
     * @return array
     */
    public function via($notifiable)
    {
        return ['broadcast', 'database'];
    }

    public function toDatabase($notifiable)
    {
        return [
            'lotes_id' => collect($this->lotes)->each(function ($lote) {
                return $lote->id;
            }),
            'title'    => 'Lotes Gerados',
            'message'  => 'Comece a dar lances, temos um novo lote em andamento'
        ];
    }

    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage([
            'id'         => $this->id,
            'read_at'    => null,
            'data'       => [
                'lotes_id' => collect($this->lotes)->each(function ($lote) {
                    return $lote->id;
                }),
                'title'    => 'Lotes Gerados',
                'message'  => 'Comece a dar lances, temos um novo lote em andamento',
            ],
            'created_at' => Carbon::now()
        ]);
    }
}
