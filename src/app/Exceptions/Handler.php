<?php

namespace App\Exceptions;

use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Support\Facades\App;
use Illuminate\Validation\ValidationException;
use Laminas\Diactoros\Response\JsonResponse;
use phpDocumentor\Reflection\Types\Parent_;
use Prettus\Validator\Exceptions\ValidatorException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Throwable  $exception
     * @return void
     *
     * @throws \Exception
     */
    public function report(Throwable $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $exception
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Throwable
     */
    public function render($request, Throwable $exception)
    {

        if ($exception instanceof ValidatorException) {
            return response()->json(error($exception, 422, trans('exceptions.validation_error')));
        }
        if ($exception instanceof AuthenticationException) {
            return response()->json(error($exception, 401, trans('auth.invalid_credentials')));
        }
        if ($exception instanceof NotFoundHttpException) {
            return response()->json(error($exception, 500, trans('exceptions.not_found_http_exception')));
        }
        if ($exception instanceof ModelNotFoundException) {
            return response()->json(error($exception, 404, trans('exceptions.resource_does_not_exist', ['model' => 'Usuário', 'key' => $exception->getIds()[0]])));
        }

        return parent::render($request, $exception);
    }

    protected function convertExceptionToArray(Throwable $e)
    {
        return error($e);
    }
}
