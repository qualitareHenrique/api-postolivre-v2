<?php

return [
    'and' => 'e',
    'unknown' => ':CAUSER interagiu.',

    'task' => [
        'task' => [
            'update' => [
                '0' => ':CAUSER alterou o(s) campo(s) :FIELDS da :CONTEXT'
            ]
        ],
        'follow' => [
            'creation' => [
                'self'   => ':CAUSER começou a seguir esta :CONTEXT .',
                'other'  => ':CAUSER adicionou :SUBJECT como seguidor desta :CONTEXT'
            ],
            'deletion' => [
                'self'   => ':CAUSER deixou de seguir esta :CONTEXT .',
                'other'  => ':CAUSER removeu :SUBJECT dos seguidores desta :CONTEXT',
            ]
        ],
        'message' => [
            'creation' => [
                '0'   => ':CAUSER comentou nesta :CONTEXT'
            ],
            'update' => [
                '0' => ':CAUSER atualizou um comentário desta :CONTEXT'
            ],
            'deletion' => [
                '0' => ':CAUSER removeu um comentário desta :CONTEXT'
            ]
        ],
        'upload' => [
            'creation' => [
                ':CAUSER anexou o arquivo :SUBJECT na :CONTEXT'
            ],
            'update' => [
                ':CAUSER atualizou o arquivo :SUBJECT da :CONTEXT'
            ],
            'deletion' => [
                ':CAUSER removeu o arquivo :SUBJECT da :CONTEXT'
            ]
        ]
    ],
    'subtask' => [
        'subtask' => [
            'update' => [
                ':CAUSER adicionou :SUBJECT como responsável por :CONTEXT',
                ':CAUSER alterou o campo :FIELD da :CONTEXT'
            ],
            'deletion' => [
                ':CAUSER excluiu a demanda :SUBJECT da :CONTEXT'
            ]
        ],
        'upload' => [
            'creation' => [
                ':CAUSER anexou o arquivo :SUBJECT na :CONTEXT'
            ],
            'update' => [
                ':CAUSER atualizou o arquivo :SUBJECT da :CONTEXT'
            ],
            'deletion' => [
                ':CAUSER removeu o arquivo :SUBJECT da :CONTEXT'
            ]
        ]
    ],
];