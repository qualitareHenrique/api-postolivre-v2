<?php

return [
    'user' => 'Usuário',
    'unit' => 'Unidade',
    'station' => 'Revendedor',
    'distributor' => 'Distribuidor',
    'port'  => 'Terminal',
];
