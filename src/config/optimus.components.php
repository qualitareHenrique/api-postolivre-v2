<?php

return [
    'namespaces' => [
        'Api' => base_path() . DIRECTORY_SEPARATOR . 'api',
        'Infrastructure' => base_path() . DIRECTORY_SEPARATOR . 'infrastructure',
        'Storage' => base_path() . DIRECTORY_SEPARATOR . 'storage/app/tenancy/tenants',
    ],


    'protection_middleware' => [
        'auth:api'
    ],

    'prefix' => '/api/v1',

    'resource_namespace' => '',

    'language_folder_name' => 'lang',

    'view_folder_name' => 'views'
];
