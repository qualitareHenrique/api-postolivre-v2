<?php
use Illuminate\Support\Facades\Route;

Route::apiResource('lot', 'LotController');
Route::get('gerar-lotes', 'LotController@gerarLotes');
