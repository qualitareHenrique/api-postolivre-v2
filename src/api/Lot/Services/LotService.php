<?php

namespace Api\Lot\Services;

use Api\Auction\Interfaces\IAuctionRepository;
use Api\Lot\Events\LotWasCreated;
use Api\Lot\Events\LotWasDeleted;
use Api\Lot\Events\LotWasUpdated;
use Api\Lot\Interfaces\ILotRepository;
use Api\Lot\Interfaces\ILotService;
use Carbon\Carbon;
use Illuminate\Database\DatabaseManager;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Events\Dispatcher;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Infrastructure\Services\Service;

class LotService extends Service implements ILotService
{
    private $auctionRepository;

    public function __construct(
        DatabaseManager $database,
        Dispatcher $dispatcher,
        ILotRepository $repository,
        IAuctionRepository $auctionRepository
    ) {
        parent::__construct($database, $dispatcher, $repository);
        $this->auctionRepository = $auctionRepository;

        $this->modelCreated  = LotWasCreated::class;
        $this->modelUpdated  = LotWasUpdated::class;
        $this->modelDeleted  = LotWasDeleted::class;
        $this->autoIncrement = true;
    }

    private function getStartDate(): Carbon
    {
        return Carbon::create(
            Carbon::now()->year,
            Carbon::now()->month,
            Carbon::now()->day, '00', '00', '00');
    }

    private function getFinishDate(): Carbon
    {
        return Carbon::create(
            Carbon::now()->year,
            Carbon::now()->month,
            Carbon::now()->day, '23', '59', '59');
    }

    public function gerarLoteDiario(): Collection
    {
        $lotesCriados = [];
        $this->database->beginTransaction();

        try {
            $this->auctionRepository
                ->fetchDailyOrders()
                ->each(function ($lote) use (&$lotesCriados) {

                    $loteCriado = $this->create([
                        'fuel_id' => $lote->fuel_id,
                        'port_id' => $lote->port_id,
                        'status'  => 'EM ANDAMENTO'
                    ]);

                    $lotesCriados[] = $loteCriado;

                    $this->auctionRepository->updateWhere([
                        ['created_at', '>=', $this->getStartDate()],
                        ['created_at', '<=', $this->getFinishDate()],
                        'fuel_id' => $lote->fuel_id,
                        'port_id' => $lote->port_id,
                        'lot_id'  => null
                    ], ['lot_id' => $loteCriado->id]);

                    $this->database->commit();
                });
        } catch (\Exception $exception) {
            $this->database->rollBack();
            Log::error("Error exception: " . $exception->getMessage());
            throw $exception;
        }

        return collect($lotesCriados);
    }
}
