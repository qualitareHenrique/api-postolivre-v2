<?php

namespace Api\Lot\Controllers;

use Api\Lot\Interfaces\ILotRepository;
use Api\Lot\Interfaces\ILotService;
use Illuminate\Database\Eloquent\Builder;
use Infrastructure\Http\CrudController;

/**
 * Class LotController
 * @package Api\Lot\Controllers
 */
class LotController extends CrudController
{
    private $lotRepository;
    /**
     * LotController constructor.
     * @param ILotService $service
     */
    public function __construct(ILotService $service, ILotRepository $lotRepository)
    {
        $this->lotRepository = $lotRepository;
        parent::__construct($service);
    }

    public function gerarLotes() {
        $lotes = $this->service->gerarLoteDiario();
        echo "Lotes gerados - " . count($lotes);
    }

    public function index()
    {
        $limit = app('request')->query('limit');
        $includes = explode(',', app('request')->query('include'));
        $search = app('request')->query('search');
        $relevancia = app('request')->query('relevancia') ?? 'nextToEnd';
        $relevancia = $this->orderByRelevance($relevancia);
        $searchFilters = $this->parserSearchData($search);
        $filtroData = $this->parseFiltroDataInterval($searchFilters);
        $filtroPosto = $this->parseFiltroPosto($searchFilters);
        $filtroCombustivel = isset($searchFilters['fuel.id']) ? $searchFilters['fuel.id'] : null;
        $this->lotRepository->skipCriteria();

        $q = $this->lotRepository->scopeQuery(function ($query) use (
            $filtroData,
            $filtroPosto,
            $filtroCombustivel,
            $limit,
            $includes,
            $relevancia
        ) {

            $q = $query
                ->join('fuels', 'fuels.id', '=', 'lots.fuel_id')
                ->join('ports', 'ports.id', '=', 'lots.port_id')
                ->join('auctions', 'auctions.lot_id', '=', 'lots.id')
                ->join('stations', 'stations.id', '=', 'auctions.station_id')
                ->join('users', 'users.id', '=', 'stations.user_id')
                ->with($includes)
                ->select('lots.*')
                ->orderBy($relevancia[0], $relevancia[1])
                ->groupBy('lots.id');


            if (isset($filtroCombustivel)) {
                $q->where('fuels.id', '=', $filtroCombustivel);
            }

            if (isset($filtroPosto)) {
                $q->where(function ($queryScope) use ($filtroPosto) {
                    $queryScope->where('users.social_reason', 'like', "%{$filtroPosto}%")
                              ->orWhere('users.cnpj', 'like', "%{$filtroPosto}%");
                });
            }

            if (isset($filtroData)) {
                $q->whereBetween('lots.created_at', $filtroData);
            }
            if ($limit) {
                $q->limit($limit);
            }
            return $q;
        });
        $data = $q->get();

        return $this->response($data);
    }

    private function orderByRelevance($relevance)
    {
        switch ($relevance) {
            case 'nextToEnd':
                return ['auctions.date_finish', 'ASC'];
                break;
            case 'lowestBidPrice':
                return ['bids.value', 'ASC'];
                break;
            case 'highestBidPrice':
                return ['bids.value', 'DESC'];
                break;
            case 'biggestQuantity':
                return ['auctions.fuel_amount', 'DESC'];
                break;
            case 'smallestQuantity':
                return ['auctions.fuel_amount', 'ASC'];
                break;
            default:
                return ['lots.id', 'DESC'];
                break;
        }
    }

    private function parserSearchData($search): array
    {
        if(empty($search)) {
            return [];
        }

        $searchData = [];

        $search = explode(',', $search);

        foreach ($search as $row) {
            list($field, $value) = explode(':', $row);
            $searchData[$field] = $value;
        }

        return $searchData;
    }

    private function parseFiltroDataInterval(array $searchFilters): ?array
    {
        $interval = $searchFilters['created_at'] ?? null;

        if(!$interval) return null;

        return explode('/', $interval);
    }

    private function parseFiltroPosto(array $searchFilters): ?string
    {
        $filter = $searchFilters['auctions.station.user.social_reason'] ?? null;

        return $filter;
    }
}
