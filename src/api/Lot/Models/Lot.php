<?php

namespace Api\Lot\Models;

use Api\Auction\Models\Auction;
use Api\Bid\Models\Bid;
use Api\Fuel\Models\Fuel;
use Api\Port\Models\Port;
use Illuminate\Database\Eloquent\SoftDeletes;
use Infrastructure\Database\Eloquent\Model;
use Infrastructure\Traits\Uuids;

class Lot extends Model
{
    use Uuids, SoftDeletes;

    public $incrementing = false;

    protected $keyType = 'uuid';

    protected $fillable = [
        'port_id',
        'fuel_id',
        'id',
        'status',
        'identifier'
    ];

    protected $casts = [
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
        'deleted_at' => 'datetime'
    ];

    public function fuel()
    {
        return $this->belongsTo(Fuel::class);
    }

    public function port()
    {
        return $this->belongsTo(Port::class);
    }

    public function auctions()
    {
        return $this->hasMany(Auction::class);
    }

    public function bids()
    {
        return $this->hasMany(Bid::class);
    }
}
