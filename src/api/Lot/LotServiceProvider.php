<?php

namespace Api\Lot;

use Api\Lot\Events\LotWasCreated;
use Api\Lot\Events\LotWasDeleted;
use Api\Lot\Events\LotWasUpdated;
use Api\Lot\Interfaces\ILotRepository;
use Api\Lot\Interfaces\ILotService;
use Api\Lot\Repositories\LotRepository;
use Api\Lot\Services\LotService;
use Infrastructure\Listeners\LogUserActivity;
use Infrastructure\Providers\EventServiceProvider;

class LotServiceProvider extends EventServiceProvider
{
    protected $listen = [
        LotWasCreated::class => [
            LogUserActivity::class
        ],
        LotWasDeleted::class => [
            LogUserActivity::class
        ],
        LotWasUpdated::class => [
            LogUserActivity::class
        ],
    ];

    public function register()
    {
        $this->app->bind(ILotService::class, LotService::class);
        $this->app->bind(ILotRepository::class, LotRepository::class);
    }
}
