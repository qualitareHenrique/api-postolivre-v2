<?php

namespace Api\Lot\Interfaces;

use Illuminate\Support\Collection;
use Infrastructure\Services\IService;

interface ILotService extends IService
{
    public function gerarLoteDiario(): Collection;
}
