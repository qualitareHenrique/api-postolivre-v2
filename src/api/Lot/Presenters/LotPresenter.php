<?php


namespace Api\Lot\Presenters;


use Api\Lot\Transformers\LotTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

class LotPresenter extends FractalPresenter
{
    protected $resourceKeyItem = 'lot';
    protected $resourceKeyCollection = 'lots';

    public function getTransformer()
    {
        return new LotTransformer();
    }
}
