<?php

namespace Api\Lot\Events;

use Api\Lot\Models\Lot;
use Infrastructure\Events\Event;

class LotWasNotConfirmed extends Event
{
    public $lot;

    public function __construct(Lot $lot)
    {
        $this->lot = $lot;
    }
}
