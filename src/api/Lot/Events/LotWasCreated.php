<?php

namespace Api\Lot\Events;

use Infrastructure\Events\Event;
use Api\Lot\Models\Lot;

class LotWasCreated extends Event
{
    public $lot;

    public function __construct(Lot $lot)
    {
        $this->lot = $lot;
    }
}
