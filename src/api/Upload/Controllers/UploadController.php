<?php

namespace Api\Upload\Controllers;

use Api\Upload\Interfaces\IUploadService;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use Infrastructure\Http\Controller;

class UploadController extends Controller
{
    protected $service;

    function __construct(IUploadService $service)
    {
        $this->service        = $service;
    }


    public function download($id, $uuidd = null)
    {
        try {

            $model = $this->service->find($id);

            if(!$model){
                throw new ModelNotFoundException();
            }

            $path = 'uploads/'. $model->filename;

            $headers = [
                'Content-Type'        => $model->mime,
                'Content-Disposition' => 'attachment; filename="'. $model->filename .'"',
            ];

            return Response::make(Storage::disk('local')->get($path), 200, $headers);

        }
        catch(\Exception $ex){
            return $this->response(error(new FileNotFoundException(), 500));
        }
    }


    /**
     * @SWG\Post(
     *     path="/uploads/download/{id}/{uuid}",
     *     summary="Download Private Files",
     *     tags={"Uploads"},
     *     description="download private files.",
     *     operationId="login",
     *     @SWG\Parameter(
     *      name="uuid",
     *      in="path",
     *      required=true,
     *      type="string",
     *     ),
     *     security={
     *      {"passport": {}},
     *     },
     *     consumes={"application/json", "application/json"},
     *     produces={"application/json", "application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Success",
     *     )
     * )
     */

}