<?php

namespace Api\Upload\Models;

use Api\Client\Models\Contact;
use Api\History\Models\History;
use Api\User\Models\User;
use Illuminate\Database\Eloquent\SoftDeletes;
use Infrastructure\Database\Eloquent\Model;
use Infrastructure\Traits\Uuids;

class Upload extends Model {

    use Uuids, SoftDeletes;

    protected $table = 'uploads';
    public $incrementing = false;

	protected $fillable = [
        'uuid',
        'extension',
	    'filename',
	    'external_url',
	    'external_bucket',
	    'external_endpoint',
	    'provider',
        'mime',
        'size',
        'is_public',
        'user_id',
        'type',
        'title',
        'description'
    ];

    protected $casts = [
        'is_public' => 'boolean',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
        'deleted_at' => 'datetime'
    ];


    public function uploadable()
    {
        return $this->morphTo();
    }

    public function uploadHistories()
    {
        return $this->morphMany(History::class, 'historiable');
    }

    public function users()
    {
        return $this->belongsTo(User::class);
    }

    public function contacts()
    {
        return $this->belongsTo(Contact::class);
    }
}
