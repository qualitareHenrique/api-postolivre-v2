<?php

namespace Api\Upload\Transformers;

use Api\History\Models\History;
use Api\History\Transformers\HistoryTransformer;
use Api\Upload\Models\Upload;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Infrastructure\Services\Upload\Providers\AbstractUploadFileService;
use League\Fractal\TransformerAbstract;

class UploadTransformer extends TransformerAbstract
{
    protected $availableIncludes = ['uploadHistories'];

    public function transform(Upload $model)
    {
//        $public_url = asset('storage/uploads/' . $model->uuid . $model->extension);
        $public_url = $_SERVER['REQUEST_SCHEME']. '://' . $_SERVER['HTTP_HOST'] . '/uploads/download/'. $model->id;

        return [
            'id'            => $model->id,
            'uuid'          => $model->uuid,
            'title'         => $model->title,
            'description'   => $model->description,
            'filename'      => $model->filename,
            'mime'          => $model->mime,
            'size'          => $model->size,
            'is_public'     => (boolean) $model->is_public,
            'type'          => $model->type,
            'extension'     => $model->extension,
            'provider'      => $model->provider,
            'external_url'  => $model->external_url ? $this->buildExternalUrl($model) : null,
            'url'           => $model->provider === 'local' ? $public_url : null,
            'created_at'    => $model->created_at
        ];
    }

    public function includeUploadHistories(Upload $model)
    {
        if ($model->uploadHistories === null) {
            return $this->null();
        }

        return $this->collection($model->uploadHistories, new HistoryTransformer(), resourceKey(History::class));
    }

    private function buildPublicUrl()
    {
        return DIRECTORY_SEPARATOR;
    }

    private function buildExternalUrl(Upload $model)
    {
        $default_region = env('AWS_DEFAULT_REGION', 'us-east-2');
        if($model->external_endpoint){
            return 'https://s3.' . $default_region . '.' . $model->external_endpoint . DIRECTORY_SEPARATOR .
                      $model->external_bucket . DIRECTORY_SEPARATOR .
                                                $model->external_url;
        }
    }
}