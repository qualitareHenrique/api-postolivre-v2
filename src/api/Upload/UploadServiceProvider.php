<?php

namespace Api\Upload;

use Api\Upload\Events\UploadPrivacyWasChanged;
use Api\Upload\Events\UploadWasCreated;
use Api\Upload\Events\UploadWasDeleted;
use Api\Upload\Events\UploadWasUpdated;
use Api\Upload\Interfaces\IUploadRepository;
use Api\Upload\Interfaces\IUploadService;
use Api\Upload\Listeners\ChangeHistoryUploadPrivacy;
use Api\Upload\Listeners\DeleteUploadHistory;
use Api\Upload\Repositories\UploadRepository;
use Api\Upload\Services\UploadService;
use Infrastructure\Listeners\LogUserActivity;
use Infrastructure\Providers\EventServiceProvider;


class UploadServiceProvider extends EventServiceProvider
{
    protected $listen = [
        UploadWasCreated::class => [
            LogUserActivity::class
        ],
        UploadWasDeleted::class => [
            LogUserActivity::class,
//            DeleteUploadHistory::class
        ],
        UploadWasUpdated::class => [
            LogUserActivity::class
        ],
        UploadPrivacyWasChanged::class => [
            ChangeHistoryUploadPrivacy::class
        ],
    ];

    public function register()
    {
        $this->app->bind(IUploadService::class, UploadService::class);
        $this->app->bind(IUploadRepository::class, UploadRepository::class);
    }
}
