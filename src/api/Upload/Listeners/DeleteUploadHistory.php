<?php

namespace Api\Upload\Listeners;

use Api\Upload\Models\Upload;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Queue\InteractsWithQueue;
use Infrastructure\Events\Event;
use Infrastructure\Services\Upload\Providers\IUploadFileService;

class DeleteUploadHistory extends Event
{
    use DispatchesJobs, InteractsWithQueue;

    private $uploadService;

    public function __construct(IUploadFileService $uploadService)
    {
        $this->uploadService = $uploadService;
    }

    /**
     * @param $event
     */
    public function handle($event)
    {
        $upload = $event->upload;
        $histories = $upload->uploadHistories;

        foreach($histories as $history){
            $attributes = json_decode($history->attributes, true);
            $model = new Upload();
            $model->fill($attributes);

            $this->uploadService->delete($model);
        }
    }
}
