<?php

namespace Api\Upload\Services;

use Api\Upload\Events\UploadWasCreated;
use Api\Upload\Events\UploadWasDeleted;
use Api\Upload\Events\UploadWasUpdated;
use Api\Upload\Interfaces\IUploadRepository;
use Api\Upload\Interfaces\IUploadService;
use Infrastructure\Database\Eloquent\Model;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Infrastructure\Services\Service;
use Illuminate\Database\DatabaseManager;
use Illuminate\Events\Dispatcher;
use Infrastructure\Services\Upload\Providers\IUploadFileService;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

class UploadService extends Service implements IUploadService
{
    private $uploadFileService;

    public function __construct(DatabaseManager $database,
                                Dispatcher $dispatcher,
                                IUploadRepository $repository,
                                IUploadFileService $uploadFileService
    )
    {
        parent::__construct($database, $dispatcher, $repository);

        $this->uploadFileService = $uploadFileService;

        $this->modelCreated = UploadWasCreated::class;
        $this->modelUpdated = UploadWasUpdated::class;
        $this->modelDeleted = UploadWasDeleted::class;
    }

    public function create(array $data, Model $model = null, $relation = null)
    {
        if($this->isMultipleFiles($data)){

            return Collection::make($this->attachMultipleFiles($data, $model));
        }

        $upload = $this->buildUniqueType($data, $model);

        return parent::create($upload, $model, 'uploads');
    }

    public function update(array $data, $id, Model $model = null)
    {
        if($model) {
            $upload = $this->buildUniqueType($data, $model);
        } else {
            $upload = $data;
        }

        return $this->runService(function() use ($upload, $data, $id){

            $upload = parent::update($upload, $id);

            $this->changeFileDirectory($upload);

            return $upload;

        }, null);
    }
    
    /**
     * Remove upload from database and storage
     *
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        //TODO: verify if skipCache still necessary
        $upload = $this->repository->skipCache(true)->find($id);

        parent::delete($id);

        $this->uploadFileService->delete($upload);

        return $upload;

    }

    /**
     * Move file from public or private directory
     *
     * @param $upload
     */
    private function changeFileDirectory($upload)
    {
        $this->uploadFileService::createBuilder()->build()->move($upload);
    }
    /**
     * Attach multiple files
     * @param array $data
     * @param Model $model
     * @return mixed
     * @internal param $upload
     * @internal param array $files
     */
    private function attachMultipleFiles(array $data, Model $model)
    {
        $attachments = [];

        foreach($data['file'] as $file){

            $current = $data;
            $current['file'] = $file;

            $upload = $this->fileBuilder($current)->save();

            $attachments[] = parent::create($upload, $model, 'uploads');
        }

        return $attachments;
    }

    /**
     * Check if file param is a array
     *
     * @param array $data
     * @return mixed
     */
    private function isMultipleFiles(array $data)
    {
        return isset($data['file']) && is_array($data['file']);
    }

    /**
     * Check for type exclusivity and Build Upload attributes array to be
     * create or updated
     *
     * @param array $data
     * @param Model $model
     * @return mixed
     */
    private function buildUniqueType(array $data, Model $model)
    {
        $this->exclusiveType($data, $model);

        return $this->fileBuilder($data)->save();
    }

    /**
     * Build array with upload attributes to be created
     *
     * @param array $data
     * @return mixed
     */
    private function fileBuilder($data)
    {
        try{

            return ($this->uploadFileService::createBuilder()
                ->setFile($data['file'])
                ->setType($data['type'] ?? null)
                ->setDescription($data['description'] ?? null)
                ->setWidth($data['width'] ?? null)
                ->setHeight($data['height'] ?? null)
                ->setUserId($data['user_id'] ?? null)
                ->setIsPublic($data['is_public'] ?? false)
                ->build());

        }catch(Exception $ex) {
            throw new FileException();
        }
    }

    /**
     * Build array with upload attributes to be created
     *
     * @param array $data
     * @param Model $model
     */
    private function exclusiveType(array $data, Model $model)
    {
        if(isset($data['type'])){
            $uploads = $this->getUploadsByType($data['type'], $model);
            $this->deletePreviousTypes($uploads);
        }
    }

    /**
     * Get upload entity by type
     *
     * @param $type
     * @param $model
     * @return mixed
     */
    private function getUploadsByType($type, Model $model)
    {
        $where = [
            'type' => $type,
            'uploadable_type' => get_class($model),
            'uploadable_id' => $model->id
        ];

        //TODO: verify if skipCache can be removed
        return $this->repository->skipCache(true)->findWhere($where);
    }

    /**
     * Remove upload database record and file
     *
     * @param $uploads
     */
    private function deletePreviousTypes($uploads)
    {
        foreach($uploads as $upload)
            $this->delete($upload->id);
    }


}