<?php

namespace Api\Unit;

use Api\Unit\Events\UnitWasCreated;
use Api\Unit\Events\UnitWasDeleted;
use Api\Unit\Events\UnitWasUpdated;
use Api\Unit\Interfaces\IUnitRepository;
use Api\Unit\Interfaces\IUnitService;
use Api\Unit\Repositories\UnitRepository;
use Api\Unit\Services\UnitService;
use Infrastructure\Listeners\LogUserActivity;
use Infrastructure\Providers\EventServiceProvider;

class UnitServiceProvider extends EventServiceProvider
{
    protected $listen = [
        UnitWasCreated::class => [
            LogUserActivity::class
        ],
        UnitWasDeleted::class => [
            LogUserActivity::class
        ],
        UnitWasUpdated::class => [
            LogUserActivity::class
        ],
    ];

    public function register()
    {
        $this->app->bind(IUnitService::class, UnitService::class);
        $this->app->bind(IUnitRepository::class, UnitRepository::class);
    }
}
