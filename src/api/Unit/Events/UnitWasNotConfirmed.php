<?php

namespace Api\Unit\Events;

use Api\Unit\Models\Unit;
use Infrastructure\Events\Event;

class UnitWasNotConfirmed extends Event
{
    public $unit;

    public function __construct(Unit $unit)
    {
        $this->unit = $unit;
    }
}
