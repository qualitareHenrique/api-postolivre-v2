<?php

namespace Api\Unit\Events;

use Infrastructure\Events\Event;
use Api\Unit\Models\Unit;

class UnitWasCreated extends Event
{
    public $unit;

    public function __construct(Unit $unit)
    {
        $this->unit = $unit;
    }
}
