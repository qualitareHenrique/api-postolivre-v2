<?php

namespace Api\Unit\Services;

use Api\Unit\Events\UnitWasCreated;
use Api\Unit\Events\UnitWasDeleted;
use Api\Unit\Events\UnitWasUpdated;
use Api\Unit\Interfaces\IUnitRepository;
use Api\Unit\Interfaces\IUnitService;
use Illuminate\Database\DatabaseManager;
use Illuminate\Events\Dispatcher;
use Infrastructure\Services\Service;

class UnitService extends Service implements IUnitService
{
    public function __construct(DatabaseManager $database, Dispatcher $dispatcher, IUnitRepository $repository)
    {
        parent::__construct($database, $dispatcher, $repository);

        $this->modelCreated = UnitWasCreated::class;
        $this->modelUpdated = UnitWasUpdated::class;
        $this->modelDeleted = UnitWasDeleted::class;
        $this->autoIncrement = true;
    }
}
