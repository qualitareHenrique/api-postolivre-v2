<?php

namespace Api\Unit\Models;

use Api\User\Models\User;
use Illuminate\Database\Eloquent\SoftDeletes;
use Infrastructure\Database\Eloquent\Model;
use Infrastructure\Traits\Uuids;

class Unit extends Model
{
    use Uuids, SoftDeletes;

    protected $keyType = 'uuid';

    protected $fillable = [
        'identifier',
        'name',
        'state_abbreviation',
        'cpf_or_cnpj',
        'phone',
        'email',
        'active'
    ];

    protected $casts = [
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
        'deleted_at' => 'datetime'
    ];

    public function users()
    {
        return $this->hasMany(User::class);
    }
}
