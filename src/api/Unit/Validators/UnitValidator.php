<?php
namespace Api\Unit\Validators;

use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\LaravelValidator;

class UnitValidator extends LaravelValidator
{
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'email' => 'required|email|between:2,50|unique:units,email,NULL,id,deleted_at,NULL',
            'name' => [
                'bail',
                'required',
                'string',
                'between:2,50',
            ],
            'state_abbreviation' => 'required|in:PB,PE,RN',
            'cpf_or_cnpj' => 'required|unique:units,cpf_or_cnpj'
        ],
        ValidatorInterface::RULE_UPDATE => [
            'email' => 'email|between:2,50|unique:users,email,NULL,id,deleted_at,NULL',
            'name' => [
                'bail',
                'string',
                'between:2,50',
            ],
            'state_abbreviation' => 'in:PB,PE,RN'
        ]
    ];

    protected $messages = [
      'email.unique' => 'Este e-mail já está sendo usado por outro usuário.'
  ];

}
