<?php
namespace Api\Notification\Validators;

use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\LaravelValidator;

class NotificationValidator extends LaravelValidator
{
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'name' => ['required', 'string'],
        ],
        ValidatorInterface::RULE_UPDATE => [
            'name' => ['string'],
        ]
    ];

}
