<?php

namespace Api\Notification\Services;

use Api\Notification\Events\NotificationWasCreated;
use Api\Notification\Events\NotificationWasDeleted;
use Api\Notification\Events\NotificationWasUpdated;
use Api\Notification\Interfaces\INotificationRepository;
use Api\Notification\Interfaces\INotificationService;
use Illuminate\Database\DatabaseManager;
use Illuminate\Events\Dispatcher;
use Infrastructure\Services\Service;

class NotificationService extends Service implements INotificationService
{
    public function __construct(DatabaseManager $database, Dispatcher $dispatcher, INotificationRepository $repository)
    {
        parent::__construct($database, $dispatcher, $repository);

        $this->modelCreated = NotificationWasCreated::class;
        $this->modelUpdated = NotificationWasUpdated::class;
        $this->modelDeleted = NotificationWasDeleted::class;
    }
}
