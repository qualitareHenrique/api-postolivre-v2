<?php

namespace Api\Notification\Repositories;

use Api\Notification\Interfaces\INotificationRepository;
use Api\Notification\Models\Notification;
use Api\Notification\Presenters\NotificationPresenter;
use Api\Notification\Validators\NotificationValidator;
use Infrastructure\Database\Eloquent\Repository;


class NotificationRepository extends Repository implements INotificationRepository
{

    protected $fieldSearchable = [
        'id',
        'name'=> 'like',
        'cpf_or_cnpj' => 'like',
    ];

    public function increment(array $columns = ['id'])
    {
        $object = $this->model->withTrashed()->pluck('identifier')->max();

        return intval($object) + 1;
    }

    public function model()
    {
        return Notification::class;
    }

    public function validator()
    {
        return NotificationValidator::class;
    }

    public function presenter()
    {
        return NotificationPresenter::class;
    }
}
