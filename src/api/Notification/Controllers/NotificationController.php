<?php

namespace Api\Notification\Controllers;

use Api\Notification\Interfaces\INotificationService;
use Infrastructure\Http\CrudController;

/**
 * Class NotificationController
 * @package Api\Notification\Controllers
 */
class NotificationController extends CrudController
{
    /**
     * NotificationController constructor.
     * @param INotificationService $service
     */
    public function __construct(INotificationService $service)
    {
        parent::__construct($service);
    }
}
