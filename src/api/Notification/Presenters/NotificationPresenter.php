<?php


namespace Api\Notification\Presenters;


use Api\Notification\Transformers\NotificationTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

class NotificationPresenter extends FractalPresenter
{
    protected $resourceKeyItem = 'notification';
    protected $resourceKeyCollection = 'notifications';

    public function getTransformer()
    {
        return new NotificationTransformer();
    }
}
