<?php

namespace Api\Bid;

use Api\Bid\Events\BidWasCreated;
use Api\Bid\Events\BidWasDeleted;
use Api\Bid\Events\BidWasUpdated;
use Api\Bid\Interfaces\IBidRepository;
use Api\Bid\Interfaces\IBidService;
use Api\Bid\Repositories\BidRepository;
use Api\Bid\Services\BidService;
use Infrastructure\Listeners\LogUserActivity;
use Infrastructure\Providers\EventServiceProvider;

class BidServiceProvider extends EventServiceProvider
{
    protected $listen = [
        BidWasCreated::class => [
            LogUserActivity::class
        ],
        BidWasDeleted::class => [
            LogUserActivity::class
        ],
        BidWasUpdated::class => [
            LogUserActivity::class
        ],
    ];

    public function register()
    {
        $this->app->bind(IBidService::class, BidService::class);
        $this->app->bind(IBidRepository::class, BidRepository::class);
    }
}
