<?php

namespace Api\Bid\Events;

use Infrastructure\Events\Event;
use Api\Bid\Models\Bid;

class BidWasUpdated extends Event
{
    public $bid;

    public function __construct(Bid $bid)
    {
        $this->bid = $bid;
    }
}
