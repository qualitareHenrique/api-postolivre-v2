<?php

namespace Api\Bid\Services;

use Api\Bid\Events\BidWasCreated;
use Api\Bid\Events\BidWasDeleted;
use Api\Bid\Events\BidWasUpdated;
use Api\Bid\Interfaces\IBidRepository;
use Api\Bid\Interfaces\IBidService;
use Api\Bid\Models\Bid;
use Api\Bid\Validators\BidValidator;
use Api\Lot\Interfaces\ILotRepository;
use Api\User\Interfaces\IUserRepository;
use App\Notifications\BidWinning;
use App\Notifications\OutdatedBid;
use Illuminate\Database\DatabaseManager;
use Illuminate\Database\Query\Builder;
use Illuminate\Events\Dispatcher;
use Illuminate\Support\Facades\Notification;
use Infrastructure\Database\Eloquent\Model;
use Infrastructure\Services\Service;

class BidService extends Service implements IBidService
{
    private $lotRepository;
    private $userRepository;

    public function __construct(
        DatabaseManager $database,
        Dispatcher $dispatcher,
        IBidRepository $repository,
        ILotRepository $lotRepository,
        IUserRepository $userRepository
    )
    {
        parent::__construct($database, $dispatcher, $repository);

        $this->lotRepository = $lotRepository;
        $this->userRepository = $userRepository;

        $this->modelCreated = BidWasCreated::class;
        $this->modelUpdated = BidWasUpdated::class;
        $this->modelDeleted = BidWasDeleted::class;
        $this->autoIncrement = true;
    }

    public function create(array $data, Model $model = null, $relation = null)
    {
        //Force validating
        validateWithData($data, BidValidator::$rulesCreate);

        // Checking notifications before creating a bid.
        // Because I had problems with transactions

        $this->notificarLanceVencedor(
            $data['value'],
            $data['lot_id'],
            $data['distributor_id']
        ) ? $this->notificarLanceUltrapassado($data['lot_id'], $data['distributor_id']) : null;
        $model = parent::create($data, $model, $relation);

        // Verificar se o lance está vencendo
        return $model;
    }

    public function menorLanceByLotId(string $lot_id)
    {
//        return $this->repository->skipCache(1)->scopeQuery(function ($query) use($lot_id) {
//            return $query
//                ->where('bids.lot_id', $lot_id)
//                ->selectRaw('MIN(bids.value) as value')
//                ->groupBy('bids.lot_id');
//        })->first();
        return Bid::query()->where('bids.lot_id', $lot_id)
            ->selectRaw('MIN(bids.value) as value')
            ->groupBy('bids.lot_id')
            ->first();
    }

    /**
     * Notifica o lance vencedor no momento que é cadastrado o lance
     */
    private function notificarLanceVencedor($lance, $loteId, $distribuidorId): bool
    {
        $menorLance = $this->menorLanceByLotId($loteId);

        if ($menorLance) {
            if ($lance < (float)($menorLance->value)) {
                // Lance que vai ser salvo está vencendo
                $user = $this->userRepository
                    ->whereHas('distributor', function ($query) use ($distribuidorId) {
                        return $query->where('id', $distribuidorId);
                    })->first();

                Notification::send($user, new BidWinning());
                return true;
            }
        }
        return false;
    }

    /**
     * @param $lotId
     * @param $distributorId
     * @TODO: Testar
     */
    private function notificarLanceUltrapassado($lotId, $distributorId)
    {
        // Lances ultrapassados
        $bidsOutdated = $this->repository->scopeQuery(function ($query) use($lotId, $distributorId) {
            return $query
                ->where('lot_id', $lotId)
                ->where('distributor_id', '!=', $distributorId)
                ->selectRaw('lot_id, distributor_id')
                ->groupBy('lot_id', 'distributor_id')
                ;
        })->get();

        collect($bidsOutdated)->each(function ($bid){
            Notification::send($bid->distributor->user, new OutdatedBid());
        });
    }
}
