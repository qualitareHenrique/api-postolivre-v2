<?php

namespace Api\Bid\Presenters;

use Api\Bid\Transformers\BidTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

class BidPresenter extends FractalPresenter
{
    protected $resourceKeyItem = 'bid';
    protected $resourceKeyCollection = 'bids';

    public function getTransformer()
    {
        return new BidTransformer();
    }
}
