<?php
namespace Api\Bid\Validators;

use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\LaravelValidator;

class BidValidator extends LaravelValidator
{
    static $rulesCreate = [
        'distributor_id' => 'required|exists:distributors,id',
        'lot_id' => 'required|exists:lots,id',
        'value' => 'required|numeric|gt:0'
    ];

    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'distributor_id' => 'required|exists:distributors,id',
            'lot_id' => 'required|exists:lots,id',
            'value' => 'required|numeric|gt:0'
        ],
        ValidatorInterface::RULE_UPDATE => [
            'distributor_id' => 'exists:distributors,id',
            'lot_id' => 'exists:lots,id',
            'value' => 'required|numeric|gt:0'
        ]
    ];

}
