<?php

namespace Api\Auction\Transformers;

use Api\Auction\Models\Auction;
use Api\Bid\Models\Bid;
use Api\Bid\Transformers\BidTransformer;
use Api\Fuel\Models\Fuel;
use Api\Fuel\Transformers\FuelTransformer;
use Api\Lot\Models\Lot;
use Api\Lot\Transformers\LotTransformer;
use Api\Port\Models\Port;
use Api\Port\Transformers\PortTransformer;
use Api\Station\Models\Station;
use Api\Station\Transformers\StationTransformer;
use Api\Upload\Models\Upload;
use Api\Upload\Transformers\UploadTransformer;
use League\Fractal\TransformerAbstract;

class AuctionTransformer extends TransformerAbstract
{
    protected $availableIncludes = ['station', 'fuel', 'bids', 'bestBid', 'port', 'uploads', 'lot'];

    public function transform(Auction $model)
    {
        return [
            'id'        => $model->id,
            'identifier'=> $model->identifier,
            'freight_type'=> $model->freight_type,
            'fuel_amount' => $model->fuel_amount,
            'date_start' => $model->date_start,
            'date_finish' => $model->date_finish,
            'payment_validated_at' => $model->payment_validated_at,
            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at,
            'deleted_at' => $model->deleted_at
        ];
    }

    public function includeStation(Auction $model)
    {
        if (null === $model->station) {
            return $this->null();
        }

        return $this->item($model->station, new StationTransformer(), resourceKey(Station::class));
    }

    public function includeFuel(Auction $model)
    {
        if (null === $model->fuel) {
            return $this->null();
        }

        return $this->item($model->fuel, new FuelTransformer(), resourceKey(Fuel::class));
    }

    public function includeBids(Auction $model)
    {
        if (null === $model->bids) {
            return $this->null();
        }

        return $this->collection($model->bids, new BidTransformer(), resourceKey(Bid::class));
    }

    public function includeBestBid(Auction $model) {
        if (null === $model->bids || collect($model->bids)->isEmpty()) {
            return $this->null();
        }
        $minValue = $model->bids()->min('value');
        $data = collect($model->bids)->filter(function ($bid) use ($minValue) {
            return $bid->value === $minValue;
        })->first();
//        dd($data, $model->bids()->selectRaw("MIN(value) as value")->first());
//        $data = $model->bids()->selectRaw("
//            MIN(value) as value
//        ")
//        ->first();

        return $this->item($data, new BidTransformer(), resourceKey(Bid::class));
    }

    public function includePort(Auction $model)
    {
        if (null === $model->port) {
            return $this->null();
        }

        return $this->item($model->port, new PortTransformer(), resourceKey(Port::class));
    }

    public function includeUploads(Auction $model)
    {
        if (null === $model->uploads) {
            return $this->null();
        }

        return $this->collection($model->uploads, new UploadTransformer(), resourceKey(Upload::class));
    }

    public function includeLot(Auction $model)
    {
        if (null === $model->lot) {
            return $this->null();
        }

        return $this->item($model->lot, new LotTransformer(), resourceKey(Lot::class));
    }
}
