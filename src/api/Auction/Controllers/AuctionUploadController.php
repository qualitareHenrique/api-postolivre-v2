<?php


namespace Api\Auction\Controllers;


use Api\Auction\Interfaces\IAuctionService;
use Api\Auction\Validators\AuctionUploadValidator;
use Api\Upload\Interfaces\IUploadService;
use Api\Upload\Validators\UploadValidator;
use Illuminate\Http\Request;
use Infrastructure\Enums\UploadTypeEnum;
use Infrastructure\Http\Controller;

class AuctionUploadController extends Controller
{
    private $uploadService;
    private $auctionService;
    private $user;

    public function __construct(IUploadService $uploadService, IAuctionService $auctionService)
    {
        $this->uploadService = $uploadService;
        $this->auctionService = $auctionService;
        $this->user = auth()->user();
    }

    /**
     * @param $uuid
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     * @uses Cadastra o comprovante de pagamento do leilão.
     */
    public function uploadPaymentVoucher($uuid, Request $request)
    {
        validate($request,AuctionUploadValidator::$arrayRules);

        $data = $request->all();

        $data['user_id'] = $this->user->id;
        $data['type'] = UploadTypeEnum::PAYMENT_VOUCHER;

        $model = $this->auctionService->find($uuid);

        $upload = $this->uploadService->create($data, $model, 'uploads');

        return $this->response($upload, $this->uploadService, 200, []);
    }
}
