<?php

namespace Api\Auction\Controllers;

use Api\Auction\Interfaces\IAuctionRepository;
use Api\Auction\Interfaces\IAuctionService;
use Illuminate\Http\Request;
use Infrastructure\Http\CrudController;

/**
 * Class FuelController
 * @package Api\Auction\Controllers
 */
class AuctionController extends CrudController
{
    private $auctionRepository;
    /**
     * FuelController constructor.
     * @param IAuctionService $service
     */
    public function __construct(IAuctionService $service, IAuctionRepository $iar)
    {
        $this->auctionRepository = $iar;
        parent::__construct($service);
    }

    public function index()
    {
        $includes = explode(',', app('request')->query('include')); 
        $search = app('request')->query('search');
        $searchFilters = $this->parserSearchData($search);
        $filtroPosto = isset($searchFilters['auctions.station.user.social_reason']) ? $searchFilters['auctions.station.user.social_reason']: null;
        $filtroCombustivel = isset($searchFilters['fuel.id']) ? $searchFilters['fuel.id'] : null;
        $this->auctionRepository->skipCriteria();
        $q = $this->auctionRepository->scopeQuery(function ($query) use (
            $includes,
            $filtroPosto,
            $searchFilters,
            $filtroCombustivel
        ) {
            $q = $query
                ->join('fuels', 'fuels.id', '=', 'auctions.fuel_id')
                ->join('stations', 'stations.id', '=', 'auctions.station_id')
                ->with($includes)
                ->select('auctions.*');

            if (isset($searchFilters['fuel.name'])) {
                $q->whereIn('fuels.name', $searchFilters['fuel.name']);
            }
            if (isset($filtroCombustivel)) {
                $q->where('fuels.id', '=', $filtroCombustivel);
            }
            if (isset($filtroPosto)) {
                $q->where(function ($queryScope) use ($filtroPosto) {
                    $queryScope->where('users.social_reason', 'ilike', "%{$filtroPosto}%")
                              ->orWhere('users.cnpj', 'ilike', "%{$filtroPosto}%");
                });
            }
            if (isset($searchFilters['created_at'])) {
                $q->whereBetween('auctions.created_at', $searchFilters['created_at']);
            }
            
            return $q;
        });
        $data = $q->get();

        return $this->response($data);
    }

    private function parserSearchData($search)
    {
        if (!$search) {
            return ['fuel.name' => null];
        }
        $searchData = [];

        $keys = [$search];
        if (stripos($search, ';')) {
            $keys = explode(';', $search);
        }
        foreach ($keys as $key) {
            $key = explode(':', $key);

            $value = [$key[1]];
            if (stripos($key[1], ',')) {
                $value = explode(',', $key[1]);
            }
            
            $searchData[$key[0]] = $value;
        }
        return $searchData;
    }
}
