<?php

namespace Api\Auction;

use Api\Auction\Events\AuctionWasCreated;
use Api\Auction\Events\AuctionWasDeleted;
use Api\Auction\Events\AuctionWasUpdated;
use Api\Auction\Interfaces\IAuctionRepository;
use Api\Auction\Interfaces\IAuctionService;
use Api\Auction\Listeners\OrderCompletion;
use Api\Auction\Repositories\AuctionRepository;
use Api\Auction\Services\AuctionService;
use Infrastructure\Listeners\LogUserActivity;
use Infrastructure\Providers\EventServiceProvider;

class AuctionServiceProvider extends EventServiceProvider
{
    protected $listen = [
        AuctionWasCreated::class => [
            LogUserActivity::class
        ],
        AuctionWasDeleted::class => [
            LogUserActivity::class
        ],
        AuctionWasUpdated::class => [
            LogUserActivity::class,
            OrderCompletion::class
        ],
    ];

    public function register()
    {
        $this->app->bind(IAuctionService::class, AuctionService::class);
        $this->app->bind(IAuctionRepository::class, AuctionRepository::class);
    }
}
