<?php

namespace Api\Auction\Events;

use Infrastructure\Events\Event;
use Api\Auction\Models\Auction;

class AuctionWasUpdated extends Event
{
    public $model;

    public function __construct(Auction $model)
    {
        $this->model = $model;
    }
}
