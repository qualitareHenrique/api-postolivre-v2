<?php

namespace Api\Auction\Events;

use Api\Auction\Models\Auction;
use Infrastructure\Events\Event;

class AuctionWasNotConfirmed extends Event
{
    public $unit;

    public function __construct(Auction $unit)
    {
        $this->unit = $unit;
    }
}
