<?php


namespace Api\Auction\Exceptions;


use Throwable;

class AuctionTimeNotAllowed extends \Exception
{
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct('Horário de leilão não permitido! Tente solicitar entre os horários de 12h às 17h', 400, $previous);
    }
}
