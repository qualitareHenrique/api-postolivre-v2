<?php
namespace Api\Auction\Validators;

use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\LaravelValidator;

class AuctionValidator extends LaravelValidator
{
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'station_id' => 'required|exists:stations,id',
            'fuel_id'   => 'required|exists:fuels,id',
            'freight_type'  => 'required|in:CIF,FOB',
            'fuel_amount'   => 'required|numeric',
            'date_start'    => 'required|date_format:Y-m-d H:i:s',
            'date_finish'   => 'required|date_format:Y-m-d H:i:s',
//            'pickup_location' => 'required|in:1,2,3',
            'port_id'       => 'required|exists:ports,id'
        ],
        ValidatorInterface::RULE_UPDATE => [
            'station_id' => 'exists:stations,id',
            'fuel_id'   => 'exists:fuels,id',
            'port_id'       => 'exists:ports,id',
            'freight_type'  => 'in:CIF,FOB',
            'fuel_amount'   => 'numeric',
            'date_start'    => 'date_format:Y-m-d H:i:s',
            'date_finish'   => 'date_format:Y-m-d H:i:s',
            'payment_validated_at' => 'date_format:Y-m-d H:i:s',
        ]
    ];

    protected $messages = [
      'freight_type.in' => 'Valor informado para frete inválido! Deve ser CIF ou FOB',
    ];

}
