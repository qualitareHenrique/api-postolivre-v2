<?php

namespace Api\Auction\Interfaces;

use Illuminate\Database\Eloquent\Collection;
use Infrastructure\Database\IRepository;

interface IAuctionRepository extends IRepository
{
    public function fetchDailyOrders(): Collection;

}
