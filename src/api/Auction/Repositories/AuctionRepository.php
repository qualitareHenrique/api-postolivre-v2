<?php

namespace Api\Auction\Repositories;

use Api\Auction\Interfaces\IAuctionRepository;
use Api\Auction\Models\Auction;
use Api\Auction\Presenters\AuctionPresenter;
use Api\Auction\Validators\AuctionValidator;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Infrastructure\Database\Eloquent\Repository;

class AuctionRepository extends Repository implements IAuctionRepository
{

    protected $fieldSearchable = [
        'id',
        'name'=> 'like',
        'freight_type',
        'fuel.name' => 'like',
        'cpf_or_cnpj' => 'like',
        'station_id',
        'fuel_id'
    ];

    public function model()
    {
        return Auction::class;
    }

    public function validator()
    {
        return AuctionValidator::class;
    }

    public function presenter()
    {
        return AuctionPresenter::class;
    }

    public function fetchDailyOrders(): Collection
    {
        return $this->scopeQuery(function (Builder $query) {
            return $query->whereBetween('created_at', [
                Carbon::yesterday()->format('Y-m-d 17:00:00'),
                Carbon::today()->format('Y-m-d 17:00:00')
            ])->whereNull('lot_id')
                         ->groupBy(['fuel_id', 'port_id']);
        })->selectRaw('fuel_id, port_id, SUM(fuel_amount)')
          ->get();
    }
}
