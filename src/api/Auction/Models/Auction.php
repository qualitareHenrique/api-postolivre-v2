<?php

namespace Api\Auction\Models;

use Api\Bid\Models\Bid;
use Api\Fuel\Models\Fuel;
use Api\Lot\Models\Lot;
use Api\Port\Models\Port;
use Api\Station\Models\Station;
use Api\Upload\Models\Upload;
use Illuminate\Database\Eloquent\SoftDeletes;
use Infrastructure\Database\Eloquent\Model;
use Infrastructure\Enums\UploadTypeEnum;
use Infrastructure\Traits\Uuids;

class Auction extends Model
{
    use Uuids, SoftDeletes;

    protected $keyType = 'uuid';

    public $incrementing = false;

    protected $casts = [
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
        'deleted_at' => 'datetime'
    ];

    protected $fillable = [
        'identifier',
        'station_id',
        'fuel_id',
        'freight_type',
        'fuel_amount',
        'date_start',
        'date_finish',
        'port_id',
        'lot_id',
        'payment_validated_at',
        'created_at'
    ];

    public function station() {
        return $this->belongsTo(Station::class);
    }

    public function fuel() {
        return $this->belongsTo(Fuel::class);
    }

    public function uploads()
    {
        return $this->morphMany(Upload::class, 'uploadable')
            ->where('type', UploadTypeEnum::PAYMENT_VOUCHER);
    }

    public function bids()
    {
        return $this->hasMany(Bid::class);
    }

    public function port()
    {
        return $this->belongsTo(Port::class);
    }

    public function lot()
    {
        return $this->belongsTo(Lot::class);
    }

}
