<?php

namespace Api\Auction\Presenters;

use Api\Auction\Transformers\AuctionTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

class AuctionPresenter extends FractalPresenter
{
    protected $resourceKeyItem = 'auction';
    protected $resourceKeyCollection = 'auctions';

    public function getTransformer()
    {
        return new AuctionTransformer();
    }
}
