<?php

namespace Api\Station\Events;

use Api\Station\Models\Station;
use Infrastructure\Events\Event;

class StationWasNotConfirmed extends Event
{
    public $unit;

    public function __construct(Station $unit)
    {
        $this->unit = $unit;
    }
}
