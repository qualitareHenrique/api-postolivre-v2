<?php
namespace Api\Station\Validators;

use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\LaravelValidator;

class StationValidator extends LaravelValidator
{
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'user_id' => ['required']
        ],
        ValidatorInterface::RULE_UPDATE => [

        ]
    ];

}
