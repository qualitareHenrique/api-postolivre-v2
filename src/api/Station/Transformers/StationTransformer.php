<?php

namespace Api\Station\Transformers;

use Api\Station\Models\Station;
use Api\Upload\Models\Upload;
use Api\Upload\Transformers\UploadTransformer;
use Api\User\Models\User;
use Api\User\Transformers\UserTransformer;
use League\Fractal\TransformerAbstract;

class StationTransformer extends TransformerAbstract
{
    protected $availableIncludes = ['uploads', 'user'];
    protected $defaultIncludes = ['uploads', 'user'];

    public function transform(Station $model)
    {
        return [
            'id' => $model->id,
            'identifier'        => $model->identifier,
            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at,
            'user_id' => $model->user_id
        ];
    }

    public function includeUploads(Station $model)
    {
        if ($model->uploads === null) {
            return $this->null();
        }

        return $this->collection($model->uploads, new UploadTransformer(), resourceKey(Upload::class));
    }

    public function includeUser(Station $model)
    {
        if (null === $model->user) {
            return $this->null();
        }

        return $this->item($model->user, new UserTransformer(), resourceKey(User::class));
    }
}
