<?php

namespace Api\Station\Controllers;

use Api\Station\Interfaces\IStationService;
use Api\Station\Models\Station;
use Api\User\Models\Address;
use Api\User\Models\BankAccount;
use Api\User\Models\User;
use Illuminate\Http\Request;
use Infrastructure\Http\CrudController;

/**
 * Class StationController
 * @package Api\Station\Controllers
 */
class StationController extends CrudController
{
    /**
     * StationController constructor.
     * @param IStationService $service
     */
    public function __construct(IStationService $service)
    {
        parent::__construct($service);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse|void
     */
    public function store(Request $request)
    {
        $address = Address::create([
            'street' => $request->get('street'),
            'city' => $request->get('city'),
            'state' => $request->get('state'),
            'number' => $request->get('number'),
            'complement' => $request->get('complement'),
            'cep' => $request->get('cep'),
            'neighborhood' => $request->get('neighborhood')
        ]);

        $bankAccount = null;
        if ($request->get('account')) {
            $bankAccount = BankAccount::create([
                'code' => $request->get('code'),
                'agency' => $request->get('agency'),
                'account' => $request->get('account'),
                'type' => $request->get('type')
            ]);
        }

        $user = User::create([
            'first_name' => $request->get('first_name'),
            'unit_id' => $request->get('unit_id'),
            'last_name' => $request->get('last_name'),
            'email' => $request->get('email'),
            'password' => $request->get('password'),
            'birthdate' => $request->get('birthdate'),
            'phone_number' => $request->get('phone_number'),
            'confirmed' => $request->get('confirmed'),
            'active' => $request->get('active'),
            'is_super' => $request->get('is_super')
        ]);

        $station = Station::create([
            'social_reason' => $request->get('social_reason'),
            'state_registration' => $request->get('state_registration'),
            'cnpj' => $request->get('cnpj'),
            'user_id' => $request->get('user_id'),
            'address_id' => $address->id,
            'bank_account_id' => $bankAccount == null ? null : $bankAccount->id
        ]);
    }
}
