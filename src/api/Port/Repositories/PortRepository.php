<?php

namespace Api\Port\Repositories;

use Api\Port\Interfaces\IPortRepository;
use Api\Port\Models\Port;
use Api\Port\Presenters\PortPresenter;
use Api\Port\Validators\PortValidator;
use Infrastructure\Database\Eloquent\Repository;


class PortRepository extends Repository implements IPortRepository
{

    protected $fieldSearchable = [
        'id',
        'name'=>'like',
    ];

    public function model()
    {
        return Port::class;
    }

    public function validator()
    {
        return PortValidator::class;
    }

    public function presenter()
    {
        return PortPresenter::class;
    }
}
