<?php

namespace Api\Port\Events;

use Api\Port\Models\Port;
use Infrastructure\Events\Event;

class PortWasNotConfirmed extends Event
{
    public $port;

    public function __construct(Port $port)
    {
        $this->port = $port;
    }
}
