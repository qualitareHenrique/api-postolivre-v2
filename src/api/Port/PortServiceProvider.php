<?php

namespace Api\Port;

use Api\Port\Events\PortWasCreated;
use Api\Port\Events\PortWasDeleted;
use Api\Port\Events\PortWasUpdated;
use Api\Port\Interfaces\IPortRepository;
use Api\Port\Interfaces\IPortService;
use Api\Port\Repositories\PortRepository;
use Api\Port\Services\PortService;
use Infrastructure\Listeners\LogUserActivity;
use Infrastructure\Providers\EventServiceProvider;

class PortServiceProvider extends EventServiceProvider
{
    protected $listen = [
        PortWasCreated::class => [
            LogUserActivity::class
        ],
        PortWasDeleted::class => [
            LogUserActivity::class
        ],
        PortWasUpdated::class => [
            LogUserActivity::class
        ],
    ];

    public function register()
    {
        $this->app->bind(IPortService::class, PortService::class);
        $this->app->bind(IPortRepository::class, PortRepository::class);
    }
}
