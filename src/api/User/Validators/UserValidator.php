<?php
namespace Api\User\Validators;

use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\LaravelValidator;

class UserValidator extends LaravelValidator
{
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'email' => 'required|email|between:2,50|unique:users,email,NULL,id,deleted_at,NULL',
            'type'      => 'required|string',
            'unit_id'   => 'string|exists:units,id',
            'password' => 'string|between:2,200',
            'phone_number' => 'string|between:2,50',
            'picture_url' => 'string|unique:users,picture_url',
            'social_reason' => 'required|unique:users,social_reason,NULL,id,deleted_at,NULL',
            'state_registration' => 'required|unique:users,state_registration,NULL,id,deleted_at,NULL',
            'cnpj' => 'required|unique:users,cnpj,NULL,id,deleted_at,NULL',

            //Address
            'address' => 'required',
            'address.street' => 'required_with:address|string',
            'address.city'   => 'required_with:address|string',
            'address.state'  => 'required_with:address|string|
                in:AC,AL,AP,AM,BA,CE,DF,ES,GO,MA,MT,MS,MG,PA,PB,PR,PE,PI,RJ,RN,RS,RO,RR,SC,SP,SE,TO',
            'address.number' => 'required_with:address|numeric',
            'address.cep'    => 'required_with:address|string',
            'address.neighborhood' => 'required_with:address|string',

            //Bank Account
            'bank.code' => 'required_if:type,distributor|required_with:bank',
            'bank.agency' => 'required_if:type,distributor|required_with:bank',
            'bank.account' => 'required_if:type,distributor|required_with:bank',
            'bank.type' => 'required_if:type,distributor|required_with:bank',

            //Files When Type equal Station
            'file' => 'required_if:type,station|array|size:2'
        ],
        ValidatorInterface::RULE_UPDATE => [
            'email' => 'email|between:2,50|unique:users,email,NULL,id,deleted_at,NULL',
            'type'      => 'sometimes|required|string',
            'unit_id'   => 'string|exists:units,id',
            'password' => 'string|between:2,200',
            'phone_number' => 'string|between:2,50',
            'picture_url' => 'string|unique:users,picture_url',
            'social_reason' => 'unique:users,social_reason,NULL,id,deleted_at,NULL',
            'state_registration' => 'unique:users,state_registration,NULL,id,deleted_at,NULL',
            'cnpj' => 'unique:users,cnpj,NULL,id,deleted_at,NULL',

            //Address
            'address' => 'sometimes|required',
            'address.street' => 'required_with:address|string',
            'address.city'   => 'required_with:address|string',
            'address.state'  => 'required_with:address|string|in:AC,AL,AP,AM,BA,CE,DF,ES,GO,
        MA,MT,MS,MG,PA,PB,PR,PE,PI,RJ,RN,RS,RO,RR,SC,SP,SE,TO',
            'address.number' => 'required_with:address|numeric',
            'address.cep'    => 'required_with:address|string',
            'address.neighborhood' => 'required_with:address|string',

            //Bank Account
            'bank.code' => 'bail|exclude_unless:type,distributor|required_if:type,distributor|min:2',
            'bank.agency' => 'bail|required_if:type,distributor',
            'bank.account' => 'bail|required_if:type,distributor',
            'bank.type' => 'bail|required_if:type,distributor',

            //Files When Type equal Station
            'file' => 'sometimes|required_if:type,station|array|size:2'
        ]
    ];

    protected $messages = [
      'email.unique' => 'Este e-mail já está sendo usado por outro usuário.'
  ];

}
