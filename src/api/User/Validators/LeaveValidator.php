<?php
namespace Api\User\Validators;

use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\LaravelValidator;

class LeaveValidator extends LaravelValidator
{
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'user_id'       => 'required',
            'leave_start'   => 'required|date',
            'leave_end'     => 'required|date',
            'reason'        => 'required|string|between:2,70',
            'active'        => 'boolean'
        ],
        ValidatorInterface::RULE_UPDATE => [
            'user_id'       => 'string',
            'leave_start'   => 'date',
            'leave_end'     => 'date',
            'reason'        => 'string|between:2,70',
            'active'        => 'boolean'
        ]
    ];


    public static $arrayRules = [
        'leave_start'   => 'required|date',
        'leave_end'     => 'required|date',
        'reason'        => 'required|string|between:2,70',
        'active'        => 'boolean'
    ];
}