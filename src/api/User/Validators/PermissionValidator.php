<?php
namespace Api\User\Validators;

use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\LaravelValidator;

class PermissionValidator extends LaravelValidator
{
    protected $rules = [
        'name' => 'string|between:2,50|unique:permissions,name',
        'label' => 'string|between:2,50|unique:permissions,label'
    ];
}