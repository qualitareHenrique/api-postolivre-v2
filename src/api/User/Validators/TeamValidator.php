<?php
namespace Api\User\Validators;

use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\LaravelValidator;

class TeamValidator extends LaravelValidator
{
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'name' => 'required|string|between:2,50|unique:teams,name,NULL,id,deleted_at,NULL',
            'user_id' => 'string|nullable',
            'properties' => 'json'
        ],

        ValidatorInterface::RULE_UPDATE => [
            'name' => 'string|between:2,50|unique:teams,name,NULL,id,deleted_at,NULL',
            'user_id' => 'string|nullable',
            'properties' => 'json'
        ]
    ];
}