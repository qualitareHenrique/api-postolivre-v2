<?php
namespace Api\User\Validators;

use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\LaravelValidator;

class RoleValidator extends LaravelValidator
{
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'name' => 'required|string|between:2,50|unique:roles,name',
//            'label' => 'required|string|between:2,50|unique:roles,label'
            'guard_name' => 'required|string'
        ],

        ValidatorInterface::RULE_UPDATE => [
            'name' => 'string|between:2,50|unique:roles,name',
            'guard_name' => 'required|string'
//            'label' => 'string|between:2,50|unique:roles,label'
        ]
    ];
}
