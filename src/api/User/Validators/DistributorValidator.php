<?php

namespace Api\User\Validators;

use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\LaravelValidator;

class DistributorValidator extends LaravelValidator
{
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [

        ],
        ValidatorInterface::RULE_UPDATE => [
            'bank_account_id' => ''
        ]
    ];
}
