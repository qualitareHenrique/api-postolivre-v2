<?php

namespace Api\User\Models;

use Api\Station\Models\Station;
use Api\Upload\Models\Upload;
use Illuminate\Auth\Authenticatable;
use Api\User\Exceptions\UserDoesNotExistException;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Notifications\Notifiable;
use Infrastructure\Database\Eloquent\Model;
use Infrastructure\Enums\UploadTypeEnum;
use Infrastructure\Traits\Uuids;
use Laravel\Passport\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements
    AuthenticatableContract,
    AuthorizableContract,
    CanResetPasswordContract
{
    use HasApiTokens,
        Notifiable,
        HasRoles,
        SoftDeletes,
        Authenticatable,
        Authorizable,
        CanResetPassword,
        Uuids;

    protected $fillable = [
        'identifier',
        'unit_id',
        'address_id',
        'social_reason',
        'state_registration',
        'cnpj',
        'email',
        'email_verified_at',
        'latest_login',
        'password',
        'phone_number',
        'confirmed',
        'active',
        'is_super'
    ];

    public $incrementing = false;
    protected $keyType = 'uuid';
    protected $hidden = [ 'password', 'remember_token' ];
    protected $guard_name = 'api';
    protected $casts = [
        'confirmed' => 'boolean',
        'active' => 'boolean',
        'is_super' => 'boolean',
        'latest_login'  => 'date',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
        'deleted_at' => 'datetime'
    ];


    /**
     * Get all of the uploads attached to this users.
     */
    public function uploads()
    {
        return $this->morphMany(Upload::class, 'uploadable');
    }

    public function address()
    {
        return $this->belongsTo(Address::class);
    }

    public function distributor()
    {
        return $this->hasOne(Distributor::class);
    }

    public function station()
    {
        return $this->hasOne(Station::class, 'user_id', 'id');
    }

	public function profilePicture()
    {
        if ($this->station) {
            return $this->station->uploads()->where('type', UploadTypeEnum::STATION_PROFILE_PICTURE);
        }

		return $this->morphOne(Upload::class, 'uploadable')->where('type', UploadTypeEnum::STATION_PROFILE_PICTURE);
	}

    /**
     * Find the user instance for the given username.
     *
     * @param  string  $username
     * @return User
     */
    public function findForPassport($username)
    {
        return $this->orWhere('cnpj', $username)->orWhere('email', $username)->first();
    }

    /**
     * Get all of the uploads attached to this users.
     */
    public function uploadedByMe(){
        $this->hasMany(Upload::class);
    }

    /**
     * Find a user by its name .
     */
    public static function findByName(string $name): User
    {
        $model = static::where('name', $name)->first();

        if (! $model) {
            throw UserDoesNotExistException::named($name);
        }

        return $model;
    }

    public static function findById($id): User
    {
        $model = static::where('id', $id)->first();

        if (! $model) {
            throw UserDoesNotExistException::withId($id);
        }

        return $model;
    }
}
