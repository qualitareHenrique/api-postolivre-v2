<?php


namespace Api\User\Models;


use Illuminate\Database\Eloquent\SoftDeletes;
use Infrastructure\Database\Eloquent\Model;
use Infrastructure\Traits\Uuids;

class BankAccount extends Model
{
    use Uuids, SoftDeletes;
    protected $table = 'bank_accounts';

    public $incrementing = false;

    protected $keyType = 'uuid';

    protected $fillable = [
        'code',
        'agency',
        'account',
        'type'
    ];
}
