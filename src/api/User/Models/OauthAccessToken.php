<?php
/**
 * Created by IntelliJ IDEA.
 * User: stalo
 * Date: 10/01/18
 * Time: 11:31
 */

namespace Api\User\Models;

use Api\Client\Models\Contact;
use Infrastructure\Database\Eloquent\Model;

class OauthAccessToken extends Model {

	protected $table = 'oauth_access_tokens';
	
	protected $fillable = ['user_id','client_id', 'name', 'scopes', 'revoked', 'expires_at'];

    public function oauthClient()
    {
        return $this->belongsTo(OauthClient::class, 'client_id');
    }
}