<?php
/**
 * Created by PhpStorm.
 * User: leonardolobato
 * Date: 27/03/18
 * Time: 11:19
 */

namespace Api\User\Models;

use Illuminate\Database\Eloquent\Relations\MorphToMany;
use Spatie\Permission\Models\Role as BaseRole;

class Role extends BaseRole
{
    protected $fillable = ['name', 'label', 'guard_name'];
    protected $hidden = [ 'api' ];

    public function getPermissions(){
        return $this->permissions;
    }

     public function users(): MorphToMany
     {
         return $this->morphedByMany(
            User::class,
            'model',
            config('permission.table_names.model_has_roles'),
            'role_id',
            'model_id'
        );
     }
}
