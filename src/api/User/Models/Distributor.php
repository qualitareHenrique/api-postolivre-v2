<?php

namespace Api\User\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Infrastructure\Database\Eloquent\Model;
use Infrastructure\Traits\Uuids;

/**
 * Class Distributor
 * @package Api\User\Models
 */
class Distributor extends Model
{
    use Uuids, SoftDeletes;

    protected $table = 'distributors';

    protected $keyType = 'uuid';

    public $incrementing = false;

    protected $fillable = [
        'user_id',
        'bank_account_id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function bankAccount()
    {
        return $this->belongsTo(BankAccount::class, 'bank_account_id');
    }

}
