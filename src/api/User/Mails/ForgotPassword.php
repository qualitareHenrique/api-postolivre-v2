<?php

namespace Api\User\Mails;

use Api\User\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ForgotPassword extends Mailable
{
    use Dispatchable, InteractsWithQueue, Queueable;

    public $user, $token;

    function __construct(User $user, $token)
    {
        $this->user  = $user;
        $this->token = $token;
    }

    public function build()
    {
        $url = config('services.email.reset_password.link');

        $email = $this->user->email;
        $logo = base64_encode(file_get_contents(resource_path('app/images/logo-pl.jpg')));

        $status = $this->view('emails.users.forgotPassword')
//            ->attachFromStorageDisk('resource','images/nilus_cover.png')
            ->with([
                'userName' => $this->user->name,
                'url' => "{$url}?token={$this->token}&email={$email}",
                'logo' => $logo
            ]);

        return $status;
    }
}
