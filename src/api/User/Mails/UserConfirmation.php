<?php

namespace Api\User\Mails;

use Api\User\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class UserConfirmation extends Mailable
{
    use Dispatchable, InteractsWithQueue, Queueable;

    protected $user;
    protected $token;

    function __construct(User $user)
    {
        $this->user  = $user;
        $this->token = $user->token;
    }

    public function build()
    {
        $url   = config('services.email.reset_password.link');
        $email = $this->user->email;

        $logo = base64_encode(file_get_contents(resource_path('app/images/logo-pl.jpg')));
        return $this->view('emails.users.userConfirmation')
                    ->subject('Confirmação de Cadastro')
                    ->with([
                        'userName' => $this->user->social_reason,
                        'url'      => "{$url}?token={$this->token}&email={$email}",
                        'logo'     => $logo
                    ]);
    }
}
