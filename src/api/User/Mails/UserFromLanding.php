<?php

namespace Api\User\Mails;

use Api\User\Models\User;
use Hyn\Tenancy\Queue\TenantAwareJob;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class UserFromLanding extends Mailable
{
    use Dispatchable, InteractsWithQueue, Queueable, TenantAwareJob;

    protected $user;

    function __construct(array $user)
    {
        $this->user  = $user;
    }

    public function build()
    {
        $logo = base64_encode(file_get_contents(resource_path('app/images/nilus_logo_clear.png')));

        return $this->view('emails.users.userFromLanding')
            ->attachFromStorageDisk('resource','images/nilus_cover.png')
            ->with([
                'logo' => $logo,
                'name' => $this->user['name'],
                'email' => $this->user['email'],
                'phone' => $this->user['phone'],
                'company' => $this->user['company'],
                'staffNumber' => $this->user['staffNumber']
            ]);
    }

}