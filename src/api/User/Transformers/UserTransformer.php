<?php

namespace Api\User\Transformers;

use Api\Station\Models\Station;
use Api\Station\Transformers\StationTransformer;
use Api\Task\Models\Timesheet;
use Api\Task\Transformers\TimesheetTransformer;
use Api\Upload\Models\Upload;
use Api\Upload\Transformers\UploadTransformer;
use Api\User\Models\Address;
use Api\User\Models\AssignmentRole;
use Api\User\Models\Distributor;
use Api\User\Models\Leave;
use Api\User\Models\Permission;
use Api\User\Models\Role;
use Api\User\Models\Team;
use Api\User\Models\User;
use Api\User\Transformers\Dashboards\PeopleDashboardTransformer;
use League\Fractal\TransformerAbstract;
use Prettus\Repository\Transformer\ModelTransformer;

//use Prettus\Transformer\BaseTransformer;

class UserTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'roles',
        'permissions',
        'address',
        'distributor',
        'station'
    ];

    public function transform(User $model)
    {
        return [
            'id'                => $model->id,
            'identifier'        => $model->identifier,
            'email'             => $model->email,
            'social_reason'     => $model->social_reason,
            'cnpj'              => $model->cnpj,
            'state_registration'=> $model->state_registration,
            'phone_number'      => $model->phone_number,
            'active'            => $model->active,
            'latest_login'      => $model->latest_login,
            'email_verified_at' => $model->email_verified_at,
            'created_at'        => $model->created_at,
            'updated_at'        => $model->updated_at,
            'deleted_at'        => $model->deleted_at,
        ];
    }

    public function includeRoles(User $model)
    {
        if ($model->roles === null) {
            return $this->null();
        }

        return $this->collection($model->roles, new RoleTransformer(), resourceKey(Role::class));
    }

    public function includePermissions(User $model)
    {
        if ($model->getAllPermissions() === null) {
            return $this->null();
        }

        return $this->collection($model->getAllPermissions(), new PermissionTransformer(), resourceKey(Permission::class));
    }

    public function includeAddress(User $model)
    {
        if ($model->address === null) {
            return $this->null();
        }

        return $this->item($model->address, new AddressTransformer(), resourceKey(Address::class));
    }

    public function includeDistributor(User $model)
    {
        if ($model->distributor === null) {
            return $this->null();
        }

        return $this->item($model->distributor, new DistributorTransformer(), resourceKey(Distributor::class));
    }

    public function includeStation(User $model)
    {
        if ($model->station === null) {
            return $this->null();
        }

        return $this->item($model->station, new StationTransformer(), resourceKey(Station::class));
    }

}
