<?php

namespace Api\User\Transformers;

use Api\User\Models\Activity;
use Illuminate\Support\Str;
use Infrastructure\Services\ActivityParser\ActivityParser;
use League\Fractal\TransformerAbstract;

class ActivityLogTransformer extends TransformerAbstract
{
    protected $availableIncludes = ['context', 'subject', 'causer'];

    public function transform(Activity $model)
    {
        $activityParser = new ActivityParser($model);

        return [
            'id'            =>  $model->id,
            'message'       =>  $activityParser->parse(),
            'created_at'    =>  $model->created_at,
        ];
    }

    public function includeContext(Activity $model){
        if ($model->context === null) {
            return $this->null();
        }

        $class = $this->getTransformerClass($model->context_type);

        return $this->item($model->context, new $class, resourceKey($class));
    }

    public function includeSubject(Activity $model)
    {
        if ($model->subject === null) {
            return $this->null();
        }

        $class = $this->getTransformerClass($model->subject_type);

        return $this->item($model->subject, new $class, resourceKey($class));
    }

    public function includeCauser(Activity $model)
    {
        if ($model->causer === null) {
            return $this->null();
        }

        $class = $this->getTransformerClass($model->causer_type);

        return $this->item($model->causer, new $class, resourceKey($class));
    }

    private function getTransformerClass($model)
    {
        return Str::replaceFirst("Models", "Transformers", $model) . "Transformer";

    }
}
