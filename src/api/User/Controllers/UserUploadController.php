<?php
/**
 * Created by PhpStorm.
 * User: leonardolobato
 * Date: 14/04/18
 * Time: 16:06
 */

namespace Api\User\Controllers;

use Api\Upload\Interfaces\IUploadService;
use Api\Upload\Validators\UploadValidator;
use Api\User\Interfaces\IUserService;
use Illuminate\Http\Request;
use Infrastructure\Http\Controller;

class UserUploadController extends Controller
{
    protected $service;
    private $uploadService;
    private $user;

    public function __construct(IUserService $service,
                                IUploadService $uploadService){
        $this->service = $service;
        $this->uploadService = $uploadService;

        $this->user = auth()->user();
    }

    public function index($id, $type = null)
    {
        if($type){
            $upload = $this->service->getUploadByType($id, $type);
            return $upload ? $this->uploadService->parserResult($upload):null;
        }

        return $this->response(
            $this->uploadService->parserResult($this->service->getUploads($id))
        );
    }

    public function store($id, $type = null, Request $request)
    {
        validate($request,UploadValidator::$arrayRules);

        $data = $request->all();

        $data['type'] = $type ?? $type;
        $data['user_id'] = $this->user->id;

        $model = $this->service->find($id);

        $this->uploadService->create($data, $model, 'uploads');

        return $this->response(
            $this->uploadService->parserResult(
                $type ?
                    $this->service->getUploadByType($id, $type):
                    $this->service->getUploads($id)
            )
        );
    }

}
