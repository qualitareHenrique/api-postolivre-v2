<?php

namespace Api\User\Controllers;

use Api\User\Interfaces\IPermissionService;
use Api\User\Models\Permission;
use Infrastructure\Http\CrudController;

class PermissionController extends CrudController
{
    public function __construct(IPermissionService $service){
        parent::__construct($service);
    }

}
