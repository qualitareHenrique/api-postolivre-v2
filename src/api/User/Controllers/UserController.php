<?php

namespace Api\User\Controllers;

use Api\User\Interfaces\IUserService;
use Api\User\Services\UserService;
use Illuminate\Database\Eloquent\Builder;
use Infrastructure\Http\CrudController;

class UserController extends CrudController
{
    /**
     * UserController constructor.
     * @param IUserService $service
     */
    public function __construct(IUserService $service){
        parent::__construct($service);
    }

    public function index()
    {
        /**
         * @var $service UserService
         */
        $service = $this->service;
        $limit = app('request')->query('limit');
        $items = $limit ?
            $service->withTrashed()->paginate($limit) : $service->withTrashed()->all();

        return $this->response($items);
    }

    /**
     * @OA\Get(
     *     path="/users",
     *     summary="Get Users",
     *     tags={"Users"},
     *     description="return a list of users",
     *     operationId="index",
     *     @OA\Parameter(
         *     name="filter",
         *     in="query",
         *     @OA\Schema(
     *           type="string",
     *         ),
     *         style="form"
     *     ),
     *     @OA\Parameter(
     *     name="limit",
     *     in="query",
     *     @OA\Schema(
     *           type="string",
     *         ),
     *         style="form"
     *     ),
     *     @OA\Parameter(
     *     name="include",
     *     in="query",
     *     @OA\Schema(
     *           type="string",
     *         ),
     *         style="form"
     *     ),
     *     @OA\Parameter(
     *     name="orderBy",
     *     in="query",
     *     @OA\Schema(
     *           type="string",
     *         ),
     *         style="form"
     *     ),
     *     @OA\Parameter(
     *     name="sortedBy",
     *     in="query",
     *     @OA\Schema(
     *           type="string",
     *         ),
     *         style="form"
     *     ),
     *     @OA\Parameter(
     *     name="search",
     *     in="query",
     *     @OA\Schema(
     *           type="string",
     *         ),
     *         style="form"
     *     ),
     *     @OA\Parameter(
     *     name="searchFields",
     *     in="query",
     *     @OA\Schema(
     *           type="string",
     *         ),
     *         style="form"
     *     ),
     *     security={
     *      {"passport": {}},
     *     },
     *     @OA\Response(
     *         response=200,
     *         description="Successful"
     *     )
     * )
     */
}
