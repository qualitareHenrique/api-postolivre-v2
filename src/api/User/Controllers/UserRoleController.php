<?php
/**
 * Created by PhpStorm.
 * User: leonardolobato
 * Date: 14/04/18
 * Time: 16:06
 */

namespace Api\User\Controllers;

use Api\User\Interfaces\IRoleService;
use Api\User\Interfaces\IUserService;
use Illuminate\Http\Request;
use Infrastructure\Http\Controller;

class UserRoleController extends Controller
{
    protected $service;
    private $roleService;

    public function __construct(IUserService $service, IRoleService $roleService){
        $this->service = $service;
        $this->roleService = $roleService;
    }

    public function index($id)
    {
        return $this->response(
            $this->roleService->parserResult(
                $this->service->getRoles($id)
            )
        );
    }

    public function store($id, Request $request)
    {
        validate($request,['roles_id' => 'array']);

        $ids = $request->get('roles_id');
        $this->service->syncRoles($id, $ids);
//dd($id,$this->service->syncRoles($id, $ids),$this->service->getRoles($id));
        return $this->response(
            $this->roleService->parserResult(
                $this->service->getRoles($id)
            )
        );
    }

}
