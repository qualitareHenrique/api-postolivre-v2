<?php

namespace Api\User\Controllers;

use Api\User\Interfaces\IActivityLogService;
use Api\User\Models\Activity;
use Infrastructure\Http\CrudController;

class ActivityLogController extends CrudController
{
    protected $service;
    protected $skipPresenter;

    public function __construct(IActivityLogService $service){
        parent::__construct($service);
    }
}
