<?php

namespace Api\User\Controllers;

use Api\User\Interfaces\IRoleService;
use Api\User\Models\Role;
use Api\User\Presenters\RolePresenter;
use Infrastructure\Http\CrudController;

class RoleController extends CrudController
{
    public function __construct(IRoleService $service){
        parent::__construct($service);
    }

}
