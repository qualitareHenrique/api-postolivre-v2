<?php

namespace Api\User\Presenters;

use Api\User\Transformers\PermissionTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

class PermissionPresenter extends FractalPresenter
{
    protected $resourceKeyItem = 'permissions';
    protected $resourceKeyCollection = 'permissions';

    public function getTransformer()
    {
        return new PermissionTransformer();
    }
}