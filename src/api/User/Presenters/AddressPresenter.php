<?php

namespace Api\User\Presenters;

use Api\User\Transformers\AddressTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

class AddressPresenter extends FractalPresenter
{
    protected $resourceKeyItem = 'address';
    protected $resourceKeyCollection = 'address';

    public function getTransformer()
    {
        return new AddressTransformer();
    }
}