<?php

namespace Api\User\Presenters;

use Api\User\Transformers\DistributorTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

class DistributorPresenter extends FractalPresenter
{
    protected $resourceKeyItem = 'distributor';
    protected $resourceKeyCollection = 'distributor';

    public function getTransformer()
    {
        return new DistributorTransformer();
    }
}