<?php

namespace Api\User\Presenters;

use Api\User\Transformers\RoleTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

class RolePresenter extends FractalPresenter
{
    protected $resourceKeyItem = 'roles';
    protected $resourceKeyCollection = 'roles';

    public function getTransformer()
    {
        return new RoleTransformer();
    }
}