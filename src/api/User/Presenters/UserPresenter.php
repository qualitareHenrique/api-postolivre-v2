<?php

namespace Api\User\Presenters;

use Api\User\Transformers\UserTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

class UserPresenter extends FractalPresenter
{
    protected $resourceKeyItem = 'users';
    protected $resourceKeyCollection = 'users';

    public function getTransformer()
    {
        return new UserTransformer();
    }
}