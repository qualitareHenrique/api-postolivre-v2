<?php

namespace Api\User\Traits;

use Api\User\Models\Team;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;

trait HasTeams
{
//    use HasMembers;

    public static function bootHasTeams()
    {
        static::deleting(function ($model) {
            if (method_exists($model, 'isForceDeleting') && ! $model->isForceDeleting()) {
                return;
            }

            $model->teams()->detach();
        });
    }


    /**
     * A model may have multiple teams.
     */
    public function teams(): MorphToMany
    {
        return $this->morphToMany(
            Team::class,
            'model',
            'model_has_teams',
            'model_id',
            'team_id'
        );
    }

    /**
     * Scope the model query to certain roles only.
     */
    public function scopeTeam(Builder $query, $teams): Builder
    {
        if ($teams instanceof Collection) {
            $teams = $teams->all();
        }

        if (! is_array($teams)) {
            $teams = [$teams];
        }

        $teams = array_map(function ($team) {
            if ($team instanceof Team) {
                return $team;
            }

            return app(Team::class)->findByName($team);
        }, $teams);

        return $query->whereHas('roles', function ($query) use ($teams) {
            $query->where(function ($query) use ($teams) {
                foreach ($teams as $team) {
                    $query->orWhere('teams.id', $team->id);
                }
            });
        });
    }

    /**
     * Assign the given team to the model.
     */
    public function assignTeam(...$teams)
    {
        $teams = collect($teams)
            ->flatten()
            ->map(function ($team) {
                return $this->getStoredTeam($team);
            })
            ->all();

        $this->teams()->saveMany($teams);

        return $this;
    }

    /**
     * Revoke the given team from the model.
     */
    public function removeTeam($team)
    {
        $this->teams()->detach($this->getStoredTeam($team));
    }

    /**
     * Remove all current teams and set the given ones.
     */
    public function syncTeams(...$teams)
    {
        $this->teams()->detach();

        return $this->assignTeam($teams);
    }

    /**
     * Determine if the model has (one of) the given teams(s).
     */
    public function hasTeam($teams): bool
    {
        if (is_string($teams) && false !== strpos($teams, '|')) {
            $teams = $this->convertPipeToArray($teams);
        }

        if (is_string($teams)) {
            return $this->teams->contains('name', $teams);
        }

        if ($teams instanceof Team) {
            return $this->teams->contains('id', $teams->id);
        }

        if (is_array($teams)) {
            foreach ($teams as $team) {
                if ($this->hasTeam($team)) {
                    return true;
                }
            }

            return false;
        }

        return $teams->intersect($this->teams)->isNotEmpty();
    }

    /**
     * Determine if the model has any of the given teams(s).
     */
    public function hasAnyTeam($teams): bool
    {
        return $this->hasTeam($teams);
    }

    /**
     * Determine if the model has all of the given team(s).
     */
    public function hasAllTeams($teams): bool
    {
        if (is_string($teams) && false !== strpos($teams, '|')) {
            $teams = $this->convertPipeToArray($teams);
        }

        if (is_string($teams)) {
            return $this->teams->contains('name', $teams);
        }

        if ($teams instanceof Team) {
            return $this->teams->contains('id', $teams->id);
        }

        $teams = collect()->make($teams)->map(function ($team) {
            return $team instanceof Team ? $team->name : $team;
        });

        return $teams->intersect($this->teams->pluck('name')) == $teams;
    }

    /**
     * Return all users directly coupled to the model.
     */
    public function getDirectMembers(): Collection
    {
        return $this->members;
    }

    public function getTeamsNames(): Collection
    {
        return $this->teams->pluck('name');
    }


    protected function getStoredTeam($team): Team
    {
        if (is_uuid($team)) {
            return app(Team::class)->findById($team, $this->getDefaultGuardName());
        }

        if (is_string($team)) {
            return app(Team::class)->findByName($team, $this->getDefaultGuardName());
        }

        return $team;
    }
}