<?php

namespace Api\User\Traits;

use Api\User\Models\User;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;

trait HasMembers
{
    public static function bootHasMembers()
    {
        static::deleting(function ($model) {
            if (method_exists($model, 'isForceDeleting') && ! $model->isForceDeleting()) {
                return;
            }

            $model->members()->detach();
        });
    }

    /**
     * A model may have multiple direct users.
     */
    public function members(): MorphToMany
    {
        return $this->morphToMany(
            User::class,
            'model',
            'model_has_users',
            'model_id',
            'users_id'
        );
    }

    /**
     * Scope the model query to certain users only.
     */
    public function scopeMember(Builder $query, $members): Builder
    {
        $members = $this->convertToUserModels($members);

        $teamsWithMembers = array_unique(array_reduce($members, function ($result, $member) {
            return array_merge($result, $member->teams->all());
        }, []));

        return $query->where(function ($query) use ($members, $teamsWithMembers) {
            $query->whereHas('users', function ($query) use ($members) {
                $query->where(function ($query) use ($members) {
                    foreach ($members as $member) {
                        $query->orWhere('users.id', $member->id);
                    }
                });
            });
            if (count($teamsWithMembers) > 0) {
                $query->orWhereHas('teams', function ($query) use ($teamsWithMembers) {
                    $query->where(function ($query) use ($teamsWithMembers) {
                        foreach ($teamsWithMembers as $team) {
                            $query->orWhere('teams.id', $team->id);
                        }
                    });
                });
            }
        });
    }


    protected function convertToUserModels($members): array
    {
        if ($members instanceof Collection) {
            $members = $members->all();
        }

        $members = array_wrap($members);

        return array_map(function ($member) {
            if ($member instanceof User) {
                return $member;
            }

            return app(User::class)->findByName($member);
        }, $members);
    }

    /**
     * Determine if the user may perform the given team.
     */
    public function canAssignTo($member):bool
    {
        if (is_string($member)) {
            $member = app(User::class)->findByName($member);
        }

        if (is_int($member)) {
            $member = app(User::class)->findById($member);
        }

        return $this->hasDirectMember($member) || $this->hasMemberViaTeam($member);
    }

    /**
     * Determine if the model has any of the given users.
     */
    public function hasAnyMember(...$members):bool
    {
        if (is_array($members[0])) {
            $members = $members[0];
        }

        foreach ($members as $member) {
            if ($this->canAssignTo($member)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Determine if the model has, via roles, the given permission.
     */
    protected function hasMemberViaTeam(User $user): bool
    {
        return $this->hasTeam($user->teams);
    }

    /**
     * Determine if the model has the given member.
     */
    public function hasDirectMember($member): bool
    {
        if (is_string($member)) {
            $member = $this->members()->where('name', '=', 2)->first();
            if (! $member) {
                return false;
            }
        }

        if (is_int($member)) {
            $member = $this->members()->where('id', '=', 2)->first();
            if (! $member) {
                return false;
            }
        }

        return $this->members->contains('id', $member->id);
    }

    /**
     * Return all the members the model has via teams.
     */
    public function getMembersViaTeams(): Collection
    {
        return $this->load('teams', 'teams.members')
            ->teams->flatMap(function ($team) {
                return $team->members;
            })->sort()->values();
    }

    /**
     * Return all the members the model has, both directly and via teams.
     */
    public function getAllMembers(): Collection
    {
        return $this->members
            ->merge($this->getMembersViaTeams())
            ->sort()
            ->values();
    }

    /**
     * Grant the given member(s) to a team.
     */
    public function giveMemberTo(...$members)
    {
        $members = collect($members)
            ->flatten()
            ->map(function ($member) {
                return $this->getStoredMember($member);
            })
            ->all();
        $this->members()->saveMany($members);

        return $this;
    }

    /**
     * Remove all current users and set the given ones.
     */
    public function syncMembers(...$members)
    {
        $this->members()->detach();

        return $this->giveMemberTo($members);
    }

    /**
     * Revoke the given member.
     */
    public function revokeMemberTo($member)
    {
        $this->members()->detach($this->getStoredMember($member));
        return $this;
    }


    protected function getStoredMember($members)
    {
        if (is_uuid($members)) {
            return app(User::class)->findById($members);
        }

        if (is_string($members)) {
            return app(User::class)->findByName($members);
        }

        if (is_array($members)) {
            return app(User::class)
                ->whereIn('name', $members)
                ->get();
        }

        return $members;
    }
}