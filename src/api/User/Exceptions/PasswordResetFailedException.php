<?php


namespace Api\User\Exceptions;

use Exception;
use Throwable;

class PasswordResetFailedException extends Exception
{

    public $user;
    public $failure;

    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    public function setFailure($failure)
    {
        $this->failure = $failure;

        return $this;
    }

}