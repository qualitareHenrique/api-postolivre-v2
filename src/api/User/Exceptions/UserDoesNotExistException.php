<?php

namespace Api\User\Exceptions;
use InvalidArgumentException;

class UserDoesNotExistException extends InvalidArgumentException
{
    public static function named(string $role_name)
    {
        return new static(trans('exceptions.there_is_entity_named', [
            'entity' => trans('exceptions.user'),
            'name' => $role_name
        ]));
    }

    public static function withId($user_id)
    {
        return new static(trans('exceptions.there_is_entity_id', [
            'entity' => trans('exceptions.user'),
            'id' => $user_id
        ]));
    }

}