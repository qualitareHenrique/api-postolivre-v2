<?php
/**
 * Created by IntelliJ IDEA.
 * User: stalo
 * Date: 22/02/18
 * Time: 17:24
 */

namespace Api\User\Repositories;

use Api\User\Interfaces\IActivityLogRepository;
use Api\User\Models\Activity;
use Api\User\Presenters\ActivityLogPresenter;
use Api\User\Validators\ActivityLogValidator;
use Infrastructure\Database\Eloquent\Repository;

class ActivityLogRepository extends Repository implements IActivityLogRepository
{
    protected $fieldSearchable = [
        'log_name', 'description' =>'like' , 'created_at'
    ];

	public function model()
	{
		return Activity::class;
	}
	
	public function validator()
	{
		return ActivityLogValidator::class;
	}

    public function presenter()
    {
        return ActivityLogPresenter::class;
    }
	
}