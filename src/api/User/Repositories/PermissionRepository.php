<?php

namespace Api\User\Repositories;

use Api\User\Interfaces\IPermissionRepository;
use Api\User\Models\Permission;
use Api\User\Presenters\PermissionPresenter;
use Api\User\Validators\PermissionValidator;
use Infrastructure\Database\Eloquent\Repository;

class PermissionRepository extends Repository implements IPermissionRepository
{

    protected $fieldSearchable = [
        'name' =>'like' ,
        'label'=>'like'
    ];

    public function model()
    {
        return Permission::class;
    }

    public function validator()
    {
        return PermissionValidator::class;
    }

    public function presenter()
    {
        return PermissionPresenter::class;
    }

}
