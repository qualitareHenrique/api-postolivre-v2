<?php

namespace Api\User\Repositories;

use Api\User\Interfaces\IAddressRepository;
use Api\User\Models\Address;
use Api\User\Presenters\AddressPresenter;
use Infrastructure\Database\Eloquent\Repository;


class AddressRepository extends Repository implements IAddressRepository
{

    protected $fieldSearchable = [
        'id',
        'state' =>'like',
        'city'  => 'like',
        'neighborhood'  => 'like',
        'country'   => 'like'
    ];

    public function model()
    {
        return Address::class;
    }

    public function validator()
    {
        return null;
    }

    public function presenter()
    {
        return AddressPresenter::class;
    }
}
