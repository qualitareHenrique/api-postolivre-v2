<?php

namespace Api\User\Repositories;

use Api\User\Interfaces\IOauthAccessTokenRepository;
use Api\User\Models\OauthAccessToken;
use Api\User\Presenters\OauthAccessTokenPresenter;
use Api\User\Validators\OauthAccessTokenValidator;
use Infrastructure\Database\Eloquent\Repository;

class OauthAccessTokenRepository extends Repository implements IOauthAccessTokenRepository {

    protected $fieldSearchable = [
    ];

	public function model()
	{
		return OauthAccessToken::class;
	}

    public function validator()
    {
        return OauthAccessTokenValidator::class;
    }

    public function presenter()
    {
        return OauthAccessTokenPresenter::class;
    }
}