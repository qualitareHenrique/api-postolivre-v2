<?php

namespace Api\User\Listeners;

use Api\User\Interfaces\IOauthAccessTokenService;
use Illuminate\Queue\InteractsWithQueue;

class RevokeAccessToken
{
    use InteractsWithQueue;

    private $accessTokenService;

    public function __construct(IOauthAccessTokenService $accessTokenService)
    {
        $this->accessTokenService = $accessTokenService;
    }

    /**
     * @param $event
     * @return bool
     * @throws \Exception
     */
    public function handle($event)
    {
        $oauth_client = $event->oauth_client;

        $this->accessTokenService->deleteWhere([
            'oauth_client_id' => $oauth_client->id
        ]);

        return true;
    }
}
