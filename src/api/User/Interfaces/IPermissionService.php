<?php
/**
 * Created by PhpStorm.
 * User: leonardolobato
 * Date: 16/01/18
 * Time: 19:13
 */

namespace Api\User\Interfaces;

use Infrastructure\Services\IService;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\MorphToMany;

interface IPermissionService extends IService
{
}