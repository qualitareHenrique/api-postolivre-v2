<?php

namespace Api\User\Events;

use Api\User\Models\OauthClient;
use Infrastructure\Events\Event;

class OauthClientWasCreated extends Event
{
    public $oauthClient;

    public function __construct(OauthClient $oauthClient)
    {
        $this->oauthClient = $oauthClient;
    }
}
