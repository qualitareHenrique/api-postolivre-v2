<?php

namespace Api\User\Events;

use Api\User\Models\Role;
use Infrastructure\Events\Event;

class RoleWasDeleted extends Event
{
    public $role;

    public function __construct(Role $role)
    {
        $this->role = $role;
    }
}
