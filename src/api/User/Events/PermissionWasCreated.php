<?php

namespace Api\User\Events;

use Api\User\Models\Permission;
use Infrastructure\Events\Event;

class PermissionWasCreated extends Event
{
    public $permission;

    public function __construct(Permission $permission)
    {
        $this->permission = $permission;
    }
}
