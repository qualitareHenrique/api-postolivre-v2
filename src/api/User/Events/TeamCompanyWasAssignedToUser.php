<?php

namespace Api\User\Events;

use Infrastructure\Events\Event;
use Api\User\Models\User;

class TeamCompanyWasAssignedToUser extends Event
{
    public $user_id;

    public function __construct($user_id)
    {
        $this->user_id = $user_id;
    }
}
