<?php

namespace Api\User\Events;

use Api\User\Models\User;
use Infrastructure\Events\Event;

class UserForgotPassword extends Event {
    public $user, $token;

    public function __construct(User $user, $token)
    {
        $this->user = $user;
        $this->token = $token;
    }
}