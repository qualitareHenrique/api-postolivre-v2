<?php
/**
 * Created by IntelliJ IDEA.
 * User: stalo
 * Date: 15/02/18
 * Time: 16:32
 */


namespace Api\User\Events;

use Api\User\Models\User;
use Infrastructure\Events\Event;

class UserWasNotConfirmed extends Event
{
    public $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }
}