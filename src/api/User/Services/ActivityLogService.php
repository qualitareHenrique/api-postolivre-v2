<?php

namespace Api\User\Services;

use Api\User\Interfaces\IActivityLogRepository;
use Api\User\Interfaces\IActivityLogService;
use Illuminate\Database\DatabaseManager;
use Illuminate\Events\Dispatcher;
use Infrastructure\Services\Service;


class ActivityLogService extends Service implements IActivityLogService
{
    public function __construct(DatabaseManager $database,
                                Dispatcher $dispatcher,
                                IActivityLogRepository $repository)
    {
        parent::__construct($database, $dispatcher, $repository);
    }


}
