<?php

namespace Api\Faq\Controllers;

use Api\Faq\Interfaces\IQuestionService;
use Infrastructure\Http\CrudController;

/**
 * Class QuestionController
 * @package Api\Faq\Controllers
 */
class QuestionController extends CrudController
{
    /**
     * QuestionController constructor.
     * @param IQuestionService $service
     */
    public function __construct(IQuestionService $service)
    {
        parent::__construct($service);
    }
}
