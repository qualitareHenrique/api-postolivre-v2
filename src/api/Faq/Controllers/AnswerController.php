<?php

namespace Api\Faq\Controllers;

use Api\Faq\Interfaces\IAnswerService;
use Infrastructure\Http\CrudController;

/**
 * Class AnswerController
 * @package Api\Faq\Controllers
 */
class AnswerController extends CrudController
{
    /**
     * AnswerController constructor.
     * @param IAnswerService $service
     */
    public function __construct(IAnswerService $service)
    {
        parent::__construct($service);
    }
}
