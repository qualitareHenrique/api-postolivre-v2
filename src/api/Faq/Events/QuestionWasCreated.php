<?php

namespace Api\Faq\Events;

use Api\Faq\Models\Question;
use Infrastructure\Events\Event;

class QuestionWasCreated extends Event
{
    public $question;

    public function __construct(Question $question)
    {
        $this->question = $question;
    }
}
