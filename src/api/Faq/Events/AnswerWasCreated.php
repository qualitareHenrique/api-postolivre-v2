<?php

namespace Api\Faq\Events;

use Api\Faq\Models\Answer;

class AnswerWasCreated
{
    public $answer;

    public function __construct(Answer $answer)
    {
        $this->answer = $answer;
    }
}