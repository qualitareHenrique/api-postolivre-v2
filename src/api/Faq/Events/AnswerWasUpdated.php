<?php

namespace Api\Faq\Events;

use Api\Faq\Models\Answer;
use Infrastructure\Events\Event;

class AnswerWasUpdated extends Event
{
    public $answer;

    public function __construct(Answer $answer)
    {
        $this->answer = $answer;
    }
}