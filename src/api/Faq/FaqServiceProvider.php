<?php

namespace Api\Faq;

use Api\Faq\Events\AnswerWasCreated;
use Api\Faq\Events\AnswerWasDeleted;
use Api\Faq\Events\AnswerWasUpdated;
use Api\Faq\Events\QuestionWasCreated;
use Api\Faq\Events\QuestionWasDeleted;
use Api\Faq\Events\QuestionWasUpdated;
use Api\Faq\Interfaces\IAnswerRepository;
use Api\Faq\Interfaces\IAnswerService;
use Api\Faq\Interfaces\IQuestionRepository;
use Api\Faq\Interfaces\IQuestionService;
use Api\Faq\Repositories\AnswerRepository;
use Api\Faq\Repositories\QuestionRepository;
use Api\Faq\Services\AnswerService;
use Api\Faq\Services\QuestionService;
use Infrastructure\Listeners\LogUserActivity;
use Infrastructure\Providers\EventServiceProvider;

class FaqServiceProvider extends EventServiceProvider
{
    protected $listen = [
        AnswerWasCreated::class => [
            LogUserActivity::class
        ],
        AnswerWasUpdated::class => [
            LogUserActivity::class
        ],
        AnswerWasDeleted::class => [
            LogUserActivity::class
        ],
        QuestionWasCreated::class => [
            LogUserActivity::class
        ],
        QuestionWasDeleted::class => [
            LogUserActivity::class
        ],
        QuestionWasUpdated::class => [
            LogUserActivity::class
        ],
    ];

    public function register()
    {
        $this->app->bind(IQuestionService::class, QuestionService::class);
        $this->app->bind(IQuestionRepository::class, QuestionRepository::class);

        $this->app->bind(IAnswerService::class, AnswerService::class);
        $this->app->bind(IAnswerRepository::class, AnswerRepository::class);
    }
}
