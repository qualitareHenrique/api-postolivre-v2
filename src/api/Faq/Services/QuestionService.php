<?php

namespace Api\Faq\Services;

use Api\Faq\Events\QuestionWasCreated;
use Api\Faq\Events\QuestionWasDeleted;
use Api\Faq\Events\QuestionWasUpdated;
use Api\Faq\Interfaces\IAnswerService;
use Api\Faq\Interfaces\IQuestionRepository;
use Api\Faq\Interfaces\IQuestionService;
use Api\Faq\Models\Question;
use Illuminate\Database\DatabaseManager;
use Illuminate\Events\Dispatcher;
use Infrastructure\Database\Eloquent\Model;
use Infrastructure\Services\Service;

class QuestionService extends Service implements IQuestionService
{
    private $answerService;

    public function __construct(
        DatabaseManager $database,
        Dispatcher $dispatcher,
        IQuestionRepository $repository,
        IAnswerService $answerService
    )
    {
        parent::__construct($database, $dispatcher, $repository);

        $this->modelCreated = QuestionWasCreated::class;
        $this->modelUpdated = QuestionWasUpdated::class;
        $this->modelDeleted = QuestionWasDeleted::class;
        $this->autoIncrement = true;
        $this->answerService = $answerService;
    }

    public function create(array $data, Model $model = null, $relation = null)
    {
        return $this->runService(function () use ($model, $relation, $data) {
            $data['user_id'] = $this->userLogged ? $this->userLogged->user()->getAuthIdentifier(): null;
            $questionModel = parent::create($data, $model, $relation);

            if (isset($data['answers'])) {
                foreach ($data['answers'] as $answer) {
                    $this->answerService->create($answer, $questionModel, 'answers');
                }
            }

            return $questionModel;
        }, $this->modelCreated);
    }

    public function update(array $data, $id)
    {
        $data['user_id'] = $this->userLogged ?
                $this->userLogged->user()->getAuthIdentifier() :
                null;
        return $this->runService(function() use ($data, $id)
        {
            /**
             * @var $model Question
             */
            $model = $this->repository->update($data,$id);

            if (isset($data['answers']) && $data['answers']) {

                $model->answers()->forceDelete();
                collect($data['answers'])->each(function ($answer) use ($id, $model) {
                    $answer['user_id'] = $this->userLogged->user()->id;
                    $answer['question_id'] = $id;
                    $answerCreated = $this->answerService->create($answer);
                    $model->answers()->save($answerCreated);
                });
            }
            return $model;

        }, $this->modelUpdated);
    }
}
