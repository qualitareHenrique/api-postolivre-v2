<?php

namespace Api\Faq\Repositories;

use Api\Faq\Interfaces\IAnswerRepository;
use Api\Faq\Interfaces\IQuestionRepository;
use Api\Faq\Models\Answer;
use Api\Faq\Presenters\AnswerPresenter;
use Api\Faq\Validators\AnswerValidator;
use Infrastructure\Database\Eloquent\Repository;


class AnswerRepository extends Repository implements IAnswerRepository
{

    protected $fieldSearchable = [
        'id',
        'answer'=>'like',
    ];

    public function model()
    {
        return Answer::class;
    }

    public function validator()
    {
        return AnswerValidator::class;
    }

    public function presenter()
    {
        return AnswerPresenter::class;
    }
}
