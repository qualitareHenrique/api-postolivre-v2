<?php

namespace Api\Faq\Models;

use Api\User\Models\User;
use Illuminate\Database\Eloquent\SoftDeletes;
use Infrastructure\Database\Eloquent\Model;
use Infrastructure\Traits\Uuids;

class Question extends Model
{
    use Uuids, SoftDeletes;

    protected $table = 'faqs_question';

    protected $keyType = 'uuid';

    protected $fillable = ['question', 'user_id', 'identifier'];

    public $incrementing = false;

    protected $casts = [
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
        'deleted_at' => 'datetime'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function answers()
    {
        return $this->hasMany(Answer::class, 'question_id');
    }
}
