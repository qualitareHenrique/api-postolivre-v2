<?php


namespace Api\Faq\Models;


use Illuminate\Database\Eloquent\SoftDeletes;
use Infrastructure\Database\Eloquent\Model;
use Infrastructure\Traits\Uuids;

class Answer extends Model
{
    use Uuids, SoftDeletes;

    protected $table = 'faqs_answer';

    protected $keyType = 'uuid';

    protected $fillable = ['answer', 'user_id', 'question_id', 'identifier'];

    public $incrementing = false;

    protected $casts = [
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
        'deleted_at' => 'datetime'
    ];


    public function question()
    {
        return $this->belongsTo(Question::class, 'question_id');
    }
}
