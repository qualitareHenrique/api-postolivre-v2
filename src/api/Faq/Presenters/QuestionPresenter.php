<?php

namespace Api\Faq\Presenters;

use Api\Faq\Transformers\QuestionTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

class QuestionPresenter extends FractalPresenter
{
    protected $resourceKeyItem = 'question';
    protected $resourceKeyCollection = 'questions';

    public function getTransformer()
    {
        return new QuestionTransformer();
    }
}
